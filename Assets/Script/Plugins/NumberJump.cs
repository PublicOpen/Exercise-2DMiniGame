﻿using System;
using System.Collections;
using UnityEngine;

public class NumberJump : MonoBehaviour
{
    private Action<float> fNumber_CallBack;
    private float fNumber = 0;
    public float pNumber
    {
        get
        {
            return fNumber;
        }
        set
        {
            fNumber = value;

            if (fNumber_CallBack != null)
            {
                fNumber_CallBack (fNumber);
            }
        }
    }
    private float fTarget_Number = 0;
    public float pTarget_Number
    {
        get
        {
            return fTarget_Number;
        }
        set
        {
            if (fAllowNegative == false && value < 0)
            {
                value = 0;
            }

            fTarget_Number = value;
        }
    }
    public float fSpacing = 1; // 每單位時間要跳動值的幅度

    public bool fAllowNegative = false;

    private Coroutine fCoroutine_NumberJumping;

    public void GetNumber (Action<float> callback)
    {
        fNumber_CallBack = callback;
    }

    public void ResetValue ()
    {
        pNumber = 0;
        pTarget_Number = 0;
    }

    public void Begin_NumberJump ()
    {
        fCoroutine_NumberJumping = StartCoroutine (_NumberJumping ());
    }

    private IEnumerator _NumberJumping ()
    {
        float temp = 0;

        while (true)
        {
            // pNumber 會一直去追 fTarget_Number的值
            if (pNumber < pTarget_Number)
            {
                temp = fNumber + fSpacing;

                if (temp > pTarget_Number)
                {
                    pNumber = pTarget_Number; // 不能高於 pTarget_Number，所以等於 pTarget_Number
                }
                else
                {
                    pNumber = temp; // 加上去趨近於目標值
                }
            }
            else if (pNumber > pTarget_Number)
            {
                temp = fNumber - fSpacing;

                if (temp < pTarget_Number)
                {
                    pNumber = pTarget_Number; // 不能低於 pTarget_Number，所以等於 pTarget_Number
                }
                else
                {
                    pNumber = temp; // 減下去趨近於目標值
                }
            }

            yield return null;
        }
    }

    public void Stop_NumberJump ()
    {
        StopCoroutine (fCoroutine_NumberJumping);
        pNumber = pTarget_Number; // 最後一定要讓 pNumber = fTarget_Number！
    }
}