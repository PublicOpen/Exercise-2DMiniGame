﻿using System;

public static class EnumEx
{
    public static T ToEnum<T> (string s)
    {
        return (T)Enum.Parse (typeof (T), s);
    }

    public static T GetRandomEnum<T> ()
    {
        Array a = Enum.GetValues (typeof (T));
        T v = (T)a.GetValue (UnityEngine.Random.Range (0, a.Length));

        return v;
    }

    public static int GetEnumLength<T> ()
    {
        return Enum.GetValues (typeof (T)).Length;
    }
}