﻿using System;
using System.Collections;
using UnityEngine;

namespace MapleStar.Tween
{
    public class TweenPosition_MultipointVersion : Tween
    {
        [Space (10)]
        public Vector3[] fMultipoint;
        private float[] fEachPathTime; // 各段路徑的執行時間
        private int fIndex; // 播放到第幾個路徑

        public void PathTimeCalculate ()
        {
            float total_pathlength = 0; // 總路徑長
            float[] pathlength = new float[fMultipoint.Length - 1]; // 各段路徑長

            /* 取得 總路徑長 與 各段路徑長(方便下面取得各段移動的時間) */
            for (int i = 0; i < pathlength.Length; i++)
            {
                pathlength[i] = Mathf.Abs (Vector3.Distance (fMultipoint[i], fMultipoint[i + 1]));
                total_pathlength += pathlength[i];
            }

            fEachPathTime = new float[pathlength.Length];

            for (int i = 0; i < fEachPathTime.Length; i++)
            {
                fEachPathTime[i] = pathlength[i] / total_pathlength * fTotal_Time;
            }
        }

        public void Play (int path, Action<TweenStatus> callback, int pathIndex = 0)
        {
            switch (fStatus)
            {
                case TweenStatus.Play:

                    fIndex = pathIndex;

                    fCurrent_Time = 0;

                    break;
                case TweenStatus.Pause:
                    StartCoroutine (_Play (callback));

                    break;
                case TweenStatus.Stop:
                    fIndex = pathIndex;
                    fCurrent_Time = 0;
                    fXYZ_AnimationCurve = fXYZ_AnimationCurves[path];
                    StartCoroutine (_Play (callback));

                    break;
            }
        }

        protected virtual IEnumerator _Play (Action<TweenStatus> callback)
        {
            /* Example */
            while (true)
            {
                fStatus = TweenStatus.Play;

                for (int i = fIndex; i < fEachPathTime.Length; i++)
                {
                    while (fCurrent_Time < fEachPathTime[i] && fStatus == TweenStatus.Play)
                    {
                        fCurrent_Time += Time.deltaTime * fSpeed;

                        float evaluate_x = fXYZ_AnimationCurve.fX.Evaluate (fCurrent_Time / fEachPathTime[i]);
                        float evaluate_y = fXYZ_AnimationCurve.fY.Evaluate (fCurrent_Time / fEachPathTime[i]);
                        float evaluate_z = fXYZ_AnimationCurve.fZ.Evaluate (fCurrent_Time / fEachPathTime[i]);

                        transform.localPosition = new Vector3 (fMultipoint[i].x + (fMultipoint[i + 1].x - fMultipoint[i].x) * evaluate_x, fMultipoint[i].y + (fMultipoint[i + 1].y - fMultipoint[i].y) * evaluate_y, fMultipoint[i].z + (fMultipoint[i + 1].z - fMultipoint[i].z) * evaluate_z);

                        yield return null;
                    }

                    if (fStatus != TweenStatus.Play)
                    {
                        break;
                    }
                    else
                    {
                        fIndex++;
                        fCurrent_Time = 0;
                    }
                }

                if (fIndex == fEachPathTime.Length)
                {
                    fStatus = TweenStatus.Stop;
                }

                callback (fStatus);

                if (fLoop == false)
                {
                    break;
                }
                else
                {
                    fIndex = 0;
                    fCurrent_Time = 0;
                }
            }
        }

        public void Back (int path, Action<TweenStatus> callback, int pathIndex = -1)
        {
            switch (fStatus)
            {
                case TweenStatus.Play:
                    if (pathIndex != -1)
                    {
                        fIndex = pathIndex;
                    }
                    else
                    {
                        fIndex = fEachPathTime.Length - 1;
                    }

                    fCurrent_Time = 0;

                    break;
                case TweenStatus.Pause:
                    StartCoroutine (_Play (callback));

                    break;
                case TweenStatus.Stop:
                    if (pathIndex != -1)
                    {
                        fIndex = pathIndex;
                    }
                    else
                    {
                        fIndex = fEachPathTime.Length - 1;
                    }

                    fCurrent_Time = 0;
                    fXYZ_AnimationCurve = fXYZ_AnimationCurves[path];
                    StartCoroutine (_Back (callback));

                    break;
            }
        }

        protected virtual IEnumerator _Back (Action<TweenStatus> callback)
        {
            /* Example */
            while (true)
            {
                fStatus = TweenStatus.Play;

                for (int i = fIndex; i >= 0; i--)
                {
                    while (fCurrent_Time < fEachPathTime[i] && fStatus == TweenStatus.Play)
                    {
                        fCurrent_Time += Time.deltaTime * fSpeed;

                        float evaluate_x = fXYZ_AnimationCurve.fX.Evaluate (fCurrent_Time / fEachPathTime[i]);
                        float evaluate_y = fXYZ_AnimationCurve.fY.Evaluate (fCurrent_Time / fEachPathTime[i]);
                        float evaluate_z = fXYZ_AnimationCurve.fZ.Evaluate (fCurrent_Time / fEachPathTime[i]);

                        transform.localPosition = new Vector3 (fMultipoint[i + 1].x + (fMultipoint[i].x - fMultipoint[i + 1].x) * evaluate_x, fMultipoint[i + 1].y + (fMultipoint[i].y - fMultipoint[i + 1].y) * evaluate_y, fMultipoint[i + 1].z + (fMultipoint[i].z - fMultipoint[i + 1].z) * evaluate_z);

                        yield return null;
                    }

                    if (fStatus != TweenStatus.Play)
                    {
                        break;
                    }
                    else
                    {
                        fIndex--;
                        fCurrent_Time = 0;
                    }
                }

                if (fIndex == -1)
                {
                    fStatus = TweenStatus.Stop;
                }

                callback (fStatus);

                if (fLoop == false)
                {
                    break;
                }
                else
                {
                    fIndex = fEachPathTime.Length - 1;
                    fCurrent_Time = 0;
                }
            }
        }

        public void Pause ()
        {
            fStatus = TweenStatus.Pause;
        }

        public void Stop ()
        {
            fStatus = TweenStatus.Stop;
        }

        public void CompletelyStop ()
        {
            fLoop = false;
            fStatus = TweenStatus.Stop;
        }
    }
}