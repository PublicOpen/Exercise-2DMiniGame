﻿using System;
using System.Collections;
using UnityEngine;

namespace MapleStar.Tween
{
    public class TweenScale : Tween
    {
        public void Play (int path, Action<TweenStatus> callback)
        {
            switch (fStatus)
            {
                case TweenStatus.Play:
                    fCurrent_Time = 0;

                    break;
                case TweenStatus.Pause:
                    StartCoroutine (_Play (callback));

                    break;
                case TweenStatus.Stop:
                    fCurrent_Time = 0;
                    fXYZ_AnimationCurve = fXYZ_AnimationCurves[path];
                    StartCoroutine (_Play (callback));

                    break;
            }
        }

        protected virtual IEnumerator _Play (Action<TweenStatus> callback)
        {
            /* Example */
            while (true)
            {
                fStatus = TweenStatus.Play;

                while (fCurrent_Time < fTotal_Time && fStatus == TweenStatus.Play)
                {
                    fCurrent_Time += Time.deltaTime * fSpeed;

                    float evaluate_x = fXYZ_AnimationCurve.fX.Evaluate (fCurrent_Time / fTotal_Time);
                    float evaluate_y = fXYZ_AnimationCurve.fY.Evaluate (fCurrent_Time / fTotal_Time);
                    float evaluate_z = fXYZ_AnimationCurve.fZ.Evaluate (fCurrent_Time / fTotal_Time);

                    transform.localScale = new Vector3 (evaluate_x, evaluate_y, evaluate_z);

                    yield return null;
                }

                if (fCurrent_Time >= fTotal_Time)
                {
                    fStatus = TweenStatus.Stop;
                }

                callback (fStatus);

                if (fLoop == false)
                {
                    break;
                }
                else
                {
                    fCurrent_Time = 0;
                }
            }
        }

        public void Back (int path, Action<TweenStatus> callback)
        {
            switch (fStatus)
            {
                case TweenStatus.Play:
                    fCurrent_Time = fTotal_Time;

                    break;
                case TweenStatus.Pause:
                    StartCoroutine (_Play (callback));

                    break;
                case TweenStatus.Stop:
                    fCurrent_Time = fTotal_Time;
                    fXYZ_AnimationCurve = fXYZ_AnimationCurves[path];
                    StartCoroutine (_Back (callback));

                    break;
            }
        }

        protected virtual IEnumerator _Back (Action<TweenStatus> callback)
        {
            /* Example */
            while (true)
            {
                fStatus = TweenStatus.Play;

                while (fCurrent_Time > 0 && fStatus == TweenStatus.Play)
                {
                    fCurrent_Time -= Time.deltaTime * fSpeed;

                    float evaluate_x = fXYZ_AnimationCurve.fX.Evaluate (fCurrent_Time / fTotal_Time);
                    float evaluate_y = fXYZ_AnimationCurve.fY.Evaluate (fCurrent_Time / fTotal_Time);
                    float evaluate_z = fXYZ_AnimationCurve.fZ.Evaluate (fCurrent_Time / fTotal_Time);

                    transform.localScale = new Vector3 (evaluate_x, evaluate_y, evaluate_z);

                    yield return null;
                }

                if (fCurrent_Time <= 0)
                {
                    fStatus = TweenStatus.Stop;
                }

                callback (fStatus);

                if (fLoop == false)
                {
                    break;
                }
                else
                {
                    fCurrent_Time = fTotal_Time;
                }
            }
        }

        public void Pause ()
        {
            fStatus = TweenStatus.Pause;
        }

        public void Stop ()
        {
            fStatus = TweenStatus.Stop;
        }

        public void CompletelyStop ()
        {
            fLoop = false;
            fStatus = TweenStatus.Stop;
        }
    }
}