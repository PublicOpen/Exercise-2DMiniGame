﻿using System;
using UnityEngine;

namespace MapleStar.Tween
{
    [Serializable]
    public class Tween : MonoBehaviour
    {
        public TweenStatus fStatus = TweenStatus.Stop;

        public float fSpeed = 1f;
        public float fTotal_Time;
        protected float fCurrent_Time;
        public bool fLoop = false;
        public XYZ_AnimationCurve[] fXYZ_AnimationCurves;
        protected XYZ_AnimationCurve fXYZ_AnimationCurve;
    }

    [Serializable]
    public struct XYZ_AnimationCurve
    {
        public AnimationCurve fX;
        public AnimationCurve fY;
        public AnimationCurve fZ;
    }

    public enum TweenStatus
    {
        Play,
        Pause,
        Stop
    }
}