﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 製作 Type T 的 class 時，
/// 記得要幫 class [System.Serializable] (包含 class 裡面的 class...)，
/// 不然會無法轉換！
/// </summary>
public class ListArrayToJson<T>
{
    public List<T> fData;

	public ListArrayToJson (List<T> list)
	{
        fData = new List<T> ();
        fData = list;
	}

	public ListArrayToJson (T[] array)
	{
        fData = new List<T> ();

		for (int i = 0; i < array.Length; i++)
		{
            fData.Add (array [i]);
		}
	}

	public string ToJson ()
	{
		return JsonUtility.ToJson (this, true);
	}
}