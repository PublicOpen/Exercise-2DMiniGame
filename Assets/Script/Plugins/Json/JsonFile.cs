﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;

/// <summary>
/// 此腳本只負責SL Json 的 string，
/// 解析資料要自己另外處理製作！
/// </summary>
public class JsonFile : Singleton<JsonFile>
{
    public void Load (string path, Action<string> callback)
    {
        StartCoroutine (_Load (path, callback));
    }

    private IEnumerator _Load (string path, Action<string> callback)
	{
		string json_data;

		#if UNITY_EDITOR || UNITY_IOS
        json_data = File.ReadAllText (path);
		#elif UNITY_ANDROID
		WWW data = new WWW (path);
		yield return data;
        json_data = data.text;
		#endif

        callback (json_data);

		yield return null;
	}

    /// <summary>
    /// WriteJsonData 注意事項：(請搭配 ListArrayToJson)
    /// 必須要在外面把資料 data 先處理好成一個 Class 類別 CustomizeType => CustomizeType data = new CustomizeType ()，
    /// 再透過 ListArrayToJson<CustomizeType> json = new ListArrayToJson<CustomizeType> (data); 轉換資料，
    /// 最後再把 (路徑, json.Tojson ()) 丟過來寫入
    /// </summary>
	public void Write (string path, string content, Action callback)
	{
		StreamWriter writer = new StreamWriter (path, false);

		writer.Write (content);
		writer.Close ();

		callback ();
	}
}