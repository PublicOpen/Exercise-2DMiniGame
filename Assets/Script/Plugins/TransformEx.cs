﻿using UnityEngine;

public static class TransformEx
{
    public static void ChildClear (Transform transform)
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy (child.gameObject);
        }
    }
}