﻿using System.Collections.Generic;

public static class ArrayListEx
{
    public static int[] DisruptSort (int lenght)
	{
        int[] index = new int[lenght];

        for (int i = 0; i < index.Length; i++)
        {
            index[i] = i;
        }

        for (int i = 0; i < index.Length; i++)
        {
            int random = UnityEngine.Random.Range (0, index.Length);

            int temp = index [random];
            index [random] = index [i];
            index [i] = temp;
        }

        return index;
	}

    public static T LastElement<T> (T[] array)
    {
        return array[array.Length - 1];
    }

    public static T LastElement<T> (List<T> list)
    {
        return list[list.Count - 1];
    }
}