﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	private static T fInstance;
	private static object fLock = new object ();
    private static bool fIs_ApplicationQuit = false;

    public static T Instance
	{
		get
        {
            if (fIs_ApplicationQuit)
			{
				return null;
			}

            lock (fLock)
			{
                if (fInstance == null)
				{
                    fInstance = (T) FindObjectOfType (typeof (T));

					if (FindObjectsOfType (typeof (T)).Length > 1)
					{
                        return fInstance;
					}

                    if (fInstance == null)
					{
						GameObject singleton = new GameObject ();
                        fInstance = singleton.AddComponent<T> ();
						singleton.name = "(Singleton) " + typeof (T);

						DontDestroyOnLoad (singleton);
					}
				}
                return fInstance;
			}
		}
	}

	public void OnDestory ()
	{
        fIs_ApplicationQuit = true;
	}
}