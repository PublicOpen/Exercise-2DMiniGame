﻿using UnityEngine;

public class ColorEx
{
    public static Color translucent = new Color (1, 1, 1, 0.5f);

    public static Color Alpha (Color color, float a)
    {
        return new Color (color.r, color.g, color.b, a);
    }

    public static Color HexColor (string code)
    {
        Color c = new Color ();

        if (ColorUtility.TryParseHtmlString (code, out c))
        {
            return c;
        }
        else
        {
            return Color.clear;
        }
    }
}