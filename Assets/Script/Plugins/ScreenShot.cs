﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;

public class ScreenShot : Singleton<ScreenShot>
{
    public string fPictureFolderPath; // 圖片存檔的資料夾路徑

    void Awake ()
    {
        fPictureFolderPath = Application.persistentDataPath + "/ScreenShot";
    }

    public void FullCatch (Action<Texture2D> callback)
    {
        StartCoroutine (_Catch (new Rect (0, 0, Screen.width, Screen.height), callback));
    }

    public void Catch (Rect rect, Action<Texture2D> callback)
    {
        StartCoroutine (_Catch (rect, callback));
    }

    private IEnumerator _Catch (Rect rect, Action<Texture2D> callback)
    {
        yield return new WaitForEndOfFrame (); // ReadPixels was called to read pixels from system frame buffer, while not inside drawing frame.

        Texture2D screen_shot = new Texture2D (Convert.ToInt32 (rect.width), Convert.ToInt32 (rect.height), TextureFormat.ARGB32, false);
        screen_shot.ReadPixels (rect, 0, 0);
        screen_shot.Apply ();

        callback (screen_shot);
    }

    public void Save (Texture2D photo, string fileName)
    {
        if (!Directory.Exists (fPictureFolderPath))
        {
            Directory.CreateDirectory (fPictureFolderPath);
        }

        string filename_extension = fileName.Split ('.')[1];// 副檔名

        switch (filename_extension.ToLower ())
        {
            case "jpg":
                File.WriteAllBytes (string.Format ("{0}/{1}", fPictureFolderPath, fileName), photo.EncodeToJPG ());

                break;
            case "png":
                File.WriteAllBytes (string.Format ("{0}/{1}", fPictureFolderPath, fileName), photo.EncodeToPNG ());

                break;
        }
    }
}