﻿using System;
using UnityEngine;

namespace MapleStar.Timer
{
    public class MultiObjectWaiting : MonoBehaviour
    {
        private int fEndCount;
        private Action fComplete;
        private int fCount;
        public int pCount
        {
            get
            {
                return fCount;
            }
            set
            {
                fCount = value;

                if (fCount == fEndCount)
                {
                    fComplete ();
                }
            }
        }

        public void Wait (int endCount, Action callback)
        {
            fCount = 0;

            fEndCount = endCount;
            fComplete = callback;
        }
    }
}