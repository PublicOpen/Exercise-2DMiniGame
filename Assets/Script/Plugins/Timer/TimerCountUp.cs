﻿using System;
using System.Collections;
using UnityEngine;

namespace MapleStar.Timer
{
    public class TimerCountUp : Timer
    {
        public void Play (Action<TimerStatus> callback)
        {
            switch (fCurrent_Status)
            {
                case TimerStatus.Play:
                    pCurrent_Time = 0;

                    break;
                case TimerStatus.Pause:
                    StartCoroutine (_Play (callback));

                    break;
                case TimerStatus.Stop:
                    pCurrent_Time = 0;
                    StartCoroutine (_Play (callback));

                    break;
            }
        }

        private IEnumerator _Play (Action<TimerStatus> callback)
        {
            fCurrent_Status = TimerStatus.Play;

            while (fCurrent_Status == TimerStatus.Play)
            {
                pCurrent_Time += Time.deltaTime * fSpeed;
                yield return null;
            }

            callback (fCurrent_Status);
        }

        public void Pause ()
        {
            fCurrent_Status = TimerStatus.Pause;
        }

        public void Stop ()
        {
            fCurrent_Status = TimerStatus.Stop;
        }
    }
}