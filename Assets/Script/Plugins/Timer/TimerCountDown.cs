﻿using System;
using System.Collections;
using UnityEngine;

namespace MapleStar.Timer
{
    public class TimerCountDown : Timer
    {
        public double fTotal_Time;

        public void Setup (TimeSpan t)
        {
            fTotal_Time = t.TotalSeconds;
        }

        public void Play (Action<TimerFinishStatus> callback)
        {
            switch (fCurrent_Status)
            {
                case TimerStatus.Play:
                    pCurrent_Time = fTotal_Time;

                    break;
                case TimerStatus.Pause:
                    StartCoroutine (_Play (callback));

                    break;
                case TimerStatus.Stop:
                    pCurrent_Time = fTotal_Time;
                    StartCoroutine (_Play (callback));

                    break;
            }
        }

        private IEnumerator _Play (Action<TimerFinishStatus> callback)
        {
            fCurrent_Status = TimerStatus.Play;

            while (pCurrent_Time > 0 && fCurrent_Status == TimerStatus.Play)
            {
                pCurrent_Time -= Time.deltaTime * fSpeed;
                yield return null;
            }

            if (pCurrent_Time <= 0)
            {
                fCurrent_Status = TimerStatus.Stop;
                callback (TimerFinishStatus.Normal);
            }
            else
            {
                callback (TimerFinishStatus.Abnormal);
            }
        }

        public void Pause ()
        {
            fCurrent_Status = TimerStatus.Pause;
        }

        public void Stop ()
        {
            fCurrent_Status = TimerStatus.Stop;
        }
    }

    public enum TimerFinishStatus
    {
        Normal,
        Abnormal
    }
}