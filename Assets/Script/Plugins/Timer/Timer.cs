﻿using System;
using UnityEngine;

namespace MapleStar.Timer
{
    public class Timer : MonoBehaviour
    {
        public TimerStatus fCurrent_Status = TimerStatus.Stop;

        public float fSpeed = 1f;
        private Action<double> fCurrent_Time_CallBack;
        private double fCurrent_Time = 0;
        public double pCurrent_Time
        {
            get
            {
                return fCurrent_Time;
            }
            set
            {
                fCurrent_Time = value;

                if (fCurrent_Time_CallBack != null)
                {
                    fCurrent_Time_CallBack (fCurrent_Time);
                }
            }
        }

        public void GetTime (Action<double> callback)
        {
            fCurrent_Time_CallBack = callback;
        }
    }

    public enum TimerStatus
    {
        Play,
        Pause,
        Stop
    }
}