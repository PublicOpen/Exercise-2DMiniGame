﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AsyncLoadScene : MonoBehaviour
{
	protected static string fTargetScene;
	protected float fLoadingProgress = 0f;

    protected void AsyncLoad ()
	{
        StartCoroutine (_AsyncLoad ());
	}

    private IEnumerator _AsyncLoad ()
	{
		AsyncOperation operation = SceneManager.LoadSceneAsync (fTargetScene);
		operation.allowSceneActivation = false; // 防止場景 Loading 完自動轉跳，operation.allowSceneActivation 設成 false 後，operation.progress 的進度只會到 0.9
		
		while (fLoadingProgress < operation.progress) 
		{
			fLoadingProgress++;

			yield return new WaitForEndOfFrame ();
		}

        yield return StartCoroutine (_ExtraAction ());

        while (fLoadingProgress < 1f)
        {
            fLoadingProgress++;

            yield return new WaitForEndOfFrame ();
        }

        operation.allowSceneActivation = true;
	}

    /// <summary>
    /// 目標場景物件讀取完成後，可以再額外做的事
    /// </summary>
    protected virtual IEnumerator _ExtraAction ()
    {
        yield return null;
    }
}