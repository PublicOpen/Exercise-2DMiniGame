﻿using MapleStar.Timer;
using MapleStar.Tween;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.WhackMole
{
    public class WhackMole_GUI : MonoBehaviour
    {
        public static WhackMole_GUI Instance;
        private GameStatus fStatus;
        public GameStatus pStatus
        {
            get
            {
                return fStatus;
            }
            set
            {
                fStatus = value;

                switch (value)
                {
                    case GameStatus.CountDown:
                        Time.timeScale = 1;

                        pTime = Convert.ToInt32 (fTime.fTotal_Time);
                        pScore = 0;

                        fPanel_CountDown.gameObject.SetActive (true);
                        fPanel_CountDown.Play (0, (TweenStatus s) => {
                            WhackMoleSystem.Instance.Switch_On_Moles ();
                            pStatus = GameStatus.Playing;
                        });

                        break;
                    case GameStatus.Playing:
                        Time.timeScale = 1;

                        fTime.Play ((TimerFinishStatus s) => {
                            if (s == TimerFinishStatus.Normal)
                            {
                                WhackMoleSystem.Instance.Switch_Off_Moles ();
                                pStatus = GameStatus.End;
                            }
                        });

                        break;
                    case GameStatus.Pause:
                        Time.timeScale = 0;

                        fTime.Pause ();

                        break;
                    case GameStatus.End:
                        fBtn_Start.interactable = true;

                        break;
                }
            }
        }

        [Header ("Header")]
        public Text fTxt_Time;
        public TimerCountDown fTime;
        private int pTime
        {
            get
            {
                return Convert.ToInt32 (fTxt_Time.text);
            }
            set
            {
                fTxt_Time.text = value.ToString ();
            }
        }
        public Text fTxt_Score;
        public int pScore
        {
            get
            {
                return Convert.ToInt32 (fTxt_Score.text);
            }
            set
            {
                fTxt_Score.text = value.ToString ();
            }
        }

        [Header ("Menu")]
        public Button fBtn_Start;

        [Header ("Panel")]
        public CountDownPanel fPanel_CountDown; // 倒數畫面
        public GameObject fPanel_Pause;

        void Awake ()
        {
            Instance = this;
        }

        void Start () 
        {
            fTime.GetTime ((double d) => {
                pTime = Convert.ToInt32 (d);
            });
            fTime.Setup (new TimeSpan (0, 0, 60));
            
            fBtn_Start.onClick.AddListener (OnClick_Start);

            pStatus = GameStatus.End;
        }

        void Update ()
        {
            /* 鍵盤事件 */
            if (Input.GetKeyDown (KeyCode.Escape))
            {
                if (pStatus == GameStatus.Playing)
                {
                    fPanel_Pause.SetActive (true);
                    pStatus = GameStatus.Pause;
                }
                else if (pStatus == GameStatus.Pause)
                {
                    fPanel_Pause.SetActive (false);
                    pStatus = GameStatus.Playing;
                }
            }
        }

        #region UI
        private void OnClick_Start ()
        {
            fBtn_Start.interactable = false;
            pStatus = GameStatus.CountDown;
        }
        #endregion
    }
}