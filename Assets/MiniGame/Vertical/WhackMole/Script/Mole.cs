﻿using MapleStar.Tween;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MapleStar.WhackMole
{
    public class Mole : TweenScale, IPointerDownHandler
    {
        [Header ("Mole")]
        public Image fImg_Self;
        public int fValue;

        void Start () 
        {
            Play (0, (TweenStatus s) => {
                if (s == TweenStatus.Stop && fImg_Self.raycastTarget == false)
                {
                    fTotal_Time = 0.1f;
                    Play (1, (TweenStatus s2) => {
                        Destroy (gameObject);
                    });
                }
                else
                {
                    Destroy (gameObject);
                }
            });
        }

        protected override IEnumerator _Play (Action<TweenStatus> callback)
        {
            while (true)
            {
                fStatus = TweenStatus.Play;

                while (fCurrent_Time < fTotal_Time && fStatus == TweenStatus.Play)
                {
                    fCurrent_Time += Time.deltaTime * fSpeed;

                    float evaluate_x = fXYZ_AnimationCurve.fX.Evaluate (fCurrent_Time / fTotal_Time);
                    float evaluate_y = fXYZ_AnimationCurve.fY.Evaluate (fCurrent_Time / fTotal_Time);

                    fImg_Self.transform.localScale = new Vector3 (evaluate_x, evaluate_y, 0);

                    yield return null;
                }

                if (fCurrent_Time >= fTotal_Time)
                {
                    fStatus = TweenStatus.Stop;
                }

                callback (fStatus);

                if (fLoop == false)
                {
                    break;
                }
                else
                {
                    fCurrent_Time = 0;
                }
            }
        }

        #region UI
        public void OnPointerDown (PointerEventData data)
        {
            fImg_Self.raycastTarget = false;
            WhackMole_GUI.Instance.pScore += fValue;

            Stop ();
        }
        #endregion
    }
}