﻿using UnityEngine;

namespace MapleStar.WhackMole
{
    public class WhackMoleSystem : MonoBehaviour
    {
        public static WhackMoleSystem Instance;

        [Header ("Hole & Mole")]
        public Hole[] fHoles;
        public GameObject[] fPrefab_Moles; // 各種地鼠

        void Awake ()
        {
            Instance = this;
        }

        public void Switch_On_Moles ()
        {
            for (int i = 0; i < fHoles.Length; i++)
            {
                fHoles[i].InvokeRepeating ("Probability", 0, 4);
            }
        }

        public void Switch_Off_Moles ()
        {
            for (int i = 0; i < fHoles.Length; i++)
            {
                fHoles[i].CancelInvoke ();

                if (fHoles[i].fMole != null)
                {
                    Destroy (fHoles[i].fMole.gameObject);
                }
            }
        }
    }
}