﻿namespace MapleStar.WhackMole
{
    public enum GameStatus
    {
        CountDown,
        Playing,
        Pause,
        End
    }
}