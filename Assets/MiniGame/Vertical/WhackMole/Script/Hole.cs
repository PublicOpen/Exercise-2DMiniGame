﻿using UnityEngine;

namespace MapleStar.WhackMole
{
    public class Hole : MonoBehaviour
    {
        public Mole fMole;

        private void Probability ()
        {
            int r = UnityEngine.Random.Range (-1, WhackMoleSystem.Instance.fPrefab_Moles.Length);

            if (r != -1)
            {
                GameObject obj = Instantiate (WhackMoleSystem.Instance.fPrefab_Moles[r], transform);
                fMole = obj.GetComponent<Mole> ();
            }
        }
    }
}