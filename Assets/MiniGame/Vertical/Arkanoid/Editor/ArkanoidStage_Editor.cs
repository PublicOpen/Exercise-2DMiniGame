﻿using Facebook.MiniJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace MapleStar.Arkanoid
{
    [CustomEditor (typeof (ArkanoidSystem))]
    public class ArkanoidSystem_Editor : Editor
    {
        private List<Level> fLevelData = new List<Level> ();
        private string fPath;

        private int fSelectionGrid_StageInt = 0;
        private string[] fStageNames; // 關卡名稱
        
        void OnEnable ()
        {
            fPath = Application.streamingAssetsPath + "/Arkanoid/StageData.dat";

            if (File.Exists (fPath))
            {
                Read_StageData ();
            }
        }

        public override void OnInspectorGUI ()
        {
            DrawDefaultInspector ();

            EditorGUILayout.Space ();
            
            GUILayout.Label ("【 ArkanoidSystemEditor Ver.1.0 】");
            if (GUILayout.Button ("New Stage"))
            {
                New_StageData ();
            }

            EditorGUILayout.Space ();

            fStageNames = new string[fLevelData.Count];

            for (int i = 0; i < fStageNames.Length; i++)
            {
                fStageNames[i] = string.Format ("{0}{1}", "Stage", i + 1);
            }

            fSelectionGrid_StageInt = GUILayout.SelectionGrid (fSelectionGrid_StageInt, fStageNames, 2);

            EditorGUILayout.Space ();

            GUILayout.BeginHorizontal ();
            if (GUILayout.Button ("Save"))
            {
                Save_StageData ();
            }
            if (GUILayout.Button ("Load"))
            {
                Load_StageData ();
            }
            GUILayout.EndHorizontal ();
        }

        private void Read_StageData ()
        {
            fLevelData.Clear ();

            string json = File.ReadAllText (fPath);
            IList datalist = (IList)((IDictionary)Json.Deserialize (json))["fData"];

            foreach (IDictionary data in datalist)
            {
                Level lv = new Level ();
                lv.fLV = Convert.ToInt32 (data["fLV"]);

                foreach (IDictionary brick in (IList)data["fBricks"])
                {
                    lv.fBricks.Add (new Brick ((BlockType)(Convert.ToInt32 (brick["fType"])), new Vector2 (Convert.ToSingle (brick["fX"]), Convert.ToSingle (brick["fY"]))));
                }
                fLevelData.Add (lv);
            }
        }

        private void New_StageData ()
        {
            Level lv = new Level ();

            Block[] blocks = GameObject.Find ("Block-Group").GetComponentsInChildren<Block> ();
            foreach (Block block in blocks)
            {
                lv.fBricks.Add (new Brick (block.fType, block.transform.localPosition));
            }

            fLevelData.Add (lv);
            lv.fLV = fLevelData.Count;

            Write_StageData ();
        }

        private void Save_StageData ()
        {
            fLevelData[fSelectionGrid_StageInt].fBricks.Clear ();

            Block[] blocks = GameObject.Find ("Block-Group").GetComponentsInChildren<Block> ();
            foreach (Block block in blocks)
            {
                fLevelData[fSelectionGrid_StageInt].fBricks.Add (new Brick (block.fType, block.transform.localPosition));
            }

            Write_StageData ();
        }

        private void Load_StageData ()
        {
            Transform block_group = GameObject.Find ("Block-Group").transform;

            foreach (Block block in block_group.GetComponentsInChildren<Block> ())
            {
                DestroyImmediate (block.gameObject);
            }

            foreach (Brick brick in fLevelData[fSelectionGrid_StageInt].fBricks)
            {
                GameObject block = Instantiate (Resources.Load ("Block") as GameObject, block_group);
                block.GetComponent<Block> ().fType = (BlockType)brick.fType;
                block.transform.localPosition = new Vector2 (brick.fX, brick.fY);
            }
        }

        private void Write_StageData ()
        {
            ListArrayToJson<Level> json = new ListArrayToJson<Level> (fLevelData);

            StreamWriter writer = new StreamWriter (fPath, false);
            writer.Write (json.ToJson ());
            writer.Close ();
            AssetDatabase.Refresh ();
        }
    }
}