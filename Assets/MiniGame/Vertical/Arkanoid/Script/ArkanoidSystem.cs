﻿using Facebook.MiniJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapleStar.Arkanoid
{
    public class ArkanoidSystem : MonoBehaviour
    {
        public static ArkanoidSystem Instance;

        public List<Level> fLevelData = new List<Level> ();
        public List<Block> fTargetBlocks = new List<Block> ();

        [Header ("Main GameObject")]
        public RectTransform fGameArea;
        public Transform fBlockGroup;
        public GameObject fPrefab_Block;
        public GameObject fPrefab_Prop;
        public Sprite[] fSprite_Props;

        [Space (10)]
        public Transform fDeadZone;

        void Awake ()
        {
            Instance = this;

            AnalysisStageData ();
        }

        private void AnalysisStageData ()
        {
            JsonFile.Instance.Load (Application.streamingAssetsPath + "/Arkanoid/StageData.dat", (string json) => {
                IList datalist = (IList)((IDictionary)Json.Deserialize (json))["fData"];

                foreach (IDictionary data in datalist)
                {
                    Level lv = new Level ();
                    lv.fLV = Convert.ToInt32 (data["fLV"]);

                    IList bricks = (IList)data["fBricks"];
                    foreach (IDictionary brick in bricks)
                    {
                        lv.fBricks.Add (new Brick ((BlockType)(Convert.ToInt32 (brick["fType"])), new Vector2 (Convert.ToSingle (brick["fX"]), Convert.ToSingle (brick["fY"]))));
                    }
                    fLevelData.Add (lv);
                }
            });
        }

        public void Setup_GameStart (int lv)
        {
            TransformEx.ChildClear (fBlockGroup);

            foreach (Brick brick in fLevelData[lv - 1].fBricks)
            {
                GameObject obj = Instantiate (fPrefab_Block, fBlockGroup);
                Block block = obj.GetComponent<Block> ();
                block.Init (brick);

                if (block.fType != BlockType.White && block.fType != BlockType.Black)
                {
                    fTargetBlocks.Add (block);
                }
            }
        }
    }
}