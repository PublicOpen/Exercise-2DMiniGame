﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MapleStar.Arkanoid
{
    [Serializable]
    public class Level
    {
        public int fLV;
        public List<Brick> fBricks = new List<Brick> ();
    }

    [Serializable]
    public struct Brick
    {
        public int fType;
        public float fX;
        public float fY;

        public Brick (BlockType type, Vector2 pos)
        {
            fType = Convert.ToInt32 (type);
            fX = pos.x;
            fY = pos.y;
        }
    }
}