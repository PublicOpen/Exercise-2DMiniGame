﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.Arkanoid
{
    public class Btn_LV : MonoBehaviour
    {
        public Button fBtn_LV;
        public Text fTxt_LV;

        void Start () 
        {
            fBtn_LV.onClick.AddListener (OnClick_LV);
        }

        void OnEnable ()
        {
            if (Convert.ToInt32 (fTxt_LV.text) <= PlayerPrefs.GetInt ("OpenLevel"))
            {
                fBtn_LV.interactable = true;
            }
        }

        #region UI
        private void OnClick_LV ()
        {
            SelectedLevelPanel.Instance.gameObject.SetActive (false);
            Arkanoid_GUI.Instance.Reset_Game (Convert.ToInt32 (fTxt_LV.text));
        }
        #endregion
    }
}