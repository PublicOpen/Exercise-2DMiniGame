﻿namespace MapleStar.Arkanoid
{
    public enum GameStatus
    {
        SelectedLevel,
        Playing,
        Pause,
        Win,
        Lose
    }

    public enum BlockType : int
    {
        Red = 1,
        Orange = 2,
        Yellow = 3,
        Green = 4,
        Blue = 5,
        Indigo = 6,
        Purple = 7,
        White, // 無敵 + 反射方向 = 入射方向
        Black // 無敵
    }

    public enum PropType
    {
        Unknown,
        Short,
        Long,
        Slow,
        Fast,
    }
}