﻿using UnityEngine;

namespace MapleStar.Arkanoid
{
    public class Ball : MonoBehaviour
    {
        public static Ball Instance;

        public Rigidbody2D fRB2D_Ball;
        private float fSpeed = 1f;
        public float pSpeed
        {
            get
            {
                return fSpeed;
            }
            set
            {
                fSpeed = value;

                if (fVelocity.x > 0 && fVelocity.y > 0)
                {
                    fVelocity = new Vector2 (fSpeed, fSpeed);
                }
                else if (fVelocity.x < 0 && fVelocity.y > 0)
                {
                    fVelocity = new Vector2 (-fSpeed, fSpeed);
                }
                else if (fVelocity.x < 0 && fVelocity.y < 0)
                {
                    fVelocity = new Vector2 (-fSpeed, -fSpeed);
                }
                else if (fVelocity.x > 0 && fVelocity.y < 0)
                {
                    fVelocity = new Vector2 (fSpeed, -fSpeed);
                }

                fRB2D_Ball.velocity = fVelocity;
            }
        }
        private Vector2 fVelocity;
        public Vector2 pVelocity
        {
            get
            {
                return fVelocity;
            }
            set
            {
                if (Mathf.Abs (value.x) < pSpeed || Mathf.Abs (value.y) < pSpeed)
                {
                    if (Mathf.Abs (value.x) < pSpeed)
                    {
                        if (fVelocity.x > 0)
                        {
                            fVelocity = new Vector2 (-pSpeed, fVelocity.y);
                        }
                        else
                        {
                            fVelocity = new Vector2 (pSpeed, fVelocity.y);
                        }
                    }

                    if (Mathf.Abs (value.y) < pSpeed)
                    {
                        if (fVelocity.y > 0)
                        {
                            fVelocity = new Vector2 (fVelocity.x, -pSpeed);
                        }
                        else
                        {
                            fVelocity = new Vector2 (fVelocity.x, pSpeed);
                        }
                    }
                }
                else
                {
                    fVelocity = value;
                }

                fRB2D_Ball.velocity = fVelocity;
            }
        }

        void Awake ()
        {
            Instance = this;
        }

        void Update ()
        {
            if (Arkanoid_GUI.Instance.pStatus == GameStatus.Playing && transform.parent == Board.Instance.transform)
            {
                transform.localPosition = new Vector2 (0, GetComponent<RectTransform> ().sizeDelta.y);;

                #if UNITY_EDITOR
                if (Input.GetKeyDown (KeyCode.Space))
                {
                    transform.SetParent (transform.parent.parent);
                    pVelocity = new Vector2 (pSpeed, pSpeed);
                }
                #elif UNITY_ANDROID || UNITY_IOS

                #endif
            }
            else
            {
                pVelocity = fRB2D_Ball.velocity;
            }
        }

        void OnDestroy ()
        {
            foreach (Prop prop in ArkanoidSystem.Instance.fGameArea.GetComponentsInChildren<Prop> ())
            {
                Destroy (prop.gameObject);
            }

            if (Arkanoid_GUI.Instance.pStatus == GameStatus.Playing)
            {
                Arkanoid_GUI.Instance.pLife--;
            }
        }
    }
}