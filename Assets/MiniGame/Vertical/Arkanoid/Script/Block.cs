﻿using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.Arkanoid
{
    public class Block : MonoBehaviour
    {
        public Image fImg_Block;

        public BlockType fType;
        private int fHP;
        public int pHP
        {
            get
            {
                return fHP;
            }
            set
            {
                fHP = value;

                if (fHP == 0)
                {
                    ArkanoidSystem.Instance.fTargetBlocks.Remove (this);

                    if (ArkanoidSystem.Instance.fTargetBlocks.Count == 0)
                    {
                        Arkanoid_GUI.Instance.pStatus = GameStatus.Win;
                    }
                    else
                    {
                        /* 掉落道具機率 10% */
                        if (UnityEngine.Random.Range (0, 100.0f) <= 10.0f)
                        {
                            GameObject obj = Instantiate (ArkanoidSystem.Instance.fPrefab_Prop, ArkanoidSystem.Instance.fBlockGroup);
                            obj.transform.localPosition = new Vector2 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y);
                        }
                    }

                    Destroy (gameObject);
                }
                else
                {
                    switch ((BlockType)fHP)
                    {
                        case BlockType.Red:
                            fImg_Block.color = new Color (1, 0, 0, 1);

                            break;
                        case BlockType.Orange:
                            fImg_Block.color = new Color (1, 0.5f, 0, 1);

                            break;
                        case BlockType.Yellow:
                            fImg_Block.color = new Color (1, 1, 0, 1);

                            break;
                        case BlockType.Green:
                            fImg_Block.color = new Color (0, 1, 0, 1);

                            break;
                        case BlockType.Blue:
                            fImg_Block.color = new Color (0, 0, 1, 1);

                            break;
                        case BlockType.Indigo:
                            fImg_Block.color = new Color (0.5f, 0, 1, 1);

                            break;
                        case BlockType.Purple:
                            fImg_Block.color = new Color (1, 0, 1, 1);

                            break;
                    }
                }
            }
        }

        public void Init (Brick brick)
        {
            fType = (BlockType)brick.fType;

            switch (fType)
            {
                case BlockType.Red:
                    fHP = 1;
                    fImg_Block.color = new Color (1, 0, 0, 1);

                    break;
                case BlockType.Orange:
                    fHP = 2;
                    fImg_Block.color = new Color (1, 0.5f, 0, 1);

                    break;
                case BlockType.Yellow:
                    fHP = 3;
                    fImg_Block.color = new Color (1, 1, 0, 1);

                    break;
                case BlockType.Green:
                    fHP = 4;
                    fImg_Block.color = new Color (0, 1, 0, 1);

                    break;
                case BlockType.Blue:
                    fHP = 5;
                    fImg_Block.color = new Color (0, 0, 1, 1);

                    break;
                case BlockType.Indigo:
                    fHP = 6;
                    fImg_Block.color = new Color (0.5f, 0, 1, 1);

                    break;
                case BlockType.Purple:
                    fHP = 7;
                    fImg_Block.color = new Color (1, 0, 1, 1);

                    break;
                case BlockType.White:
                    fImg_Block.color = Color.white;

                    break;
                case BlockType.Black:
                    fImg_Block.color = Color.black;

                    break;
            }

            transform.localPosition = new Vector2 (brick.fX, brick.fY);
        }

        void OnCollisionEnter2D (Collision2D other)
        {
            if (other.gameObject.GetComponent<Ball> ())
            {
                switch (fType)
                {
                    case BlockType.White:
                        other.gameObject.GetComponent<Ball> ().pVelocity *= -1;

                        break;
                    case BlockType.Black:

                        break;
                    default:
                        pHP--;

                        break;
                }
            }
        }
    }
}