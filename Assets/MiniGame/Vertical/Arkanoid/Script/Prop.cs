﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.Arkanoid
{
    public class Prop : MonoBehaviour
    {
        public Rigidbody2D fRB2D_Prop;
        private PropType fType;

        void Start ()
        {
            fRB2D_Prop.velocity = new Vector2 (0, -0.1f);
            fType = EnumEx.GetRandomEnum<PropType> ();
            GetComponent<Image> ().sprite = ArkanoidSystem.Instance.fSprite_Props[Convert.ToInt32 (fType)];
        }

        void OnTriggerEnter2D (Collider2D other)
        {
            if (other.GetComponent<Board> ())
            {
                PropEffect (fType);
            }
        }

        private void PropEffect (PropType type)
        {
            switch (type)
            {
                case PropType.Unknown:
                    PropEffect (EnumEx.GetRandomEnum<PropType> ());

                    break;
                case PropType.Short:
                    if (Board.Instance.GetComponent<RectTransform> ().sizeDelta.x > 150)
                    {
                        Board.Instance.GetComponent<RectTransform> ().sizeDelta = new Vector2 (Board.Instance.GetComponent<RectTransform> ().sizeDelta.x - 20, Board.Instance.GetComponent<RectTransform> ().sizeDelta.y);
                    }

                    break;
                case PropType.Long:
                    if (Board.Instance.GetComponent<RectTransform> ().sizeDelta.x < 350)
                    {
                        Board.Instance.GetComponent<RectTransform> ().sizeDelta = new Vector2 (Board.Instance.GetComponent<RectTransform> ().sizeDelta.x + 20, Board.Instance.GetComponent<RectTransform> ().sizeDelta.y);
                    }

                    break;
                case PropType.Slow:
                    if (Ball.Instance.pSpeed > 0.2f)
                    {
                        Ball.Instance.pSpeed -= 0.2f;
                    }

                    break;
                case PropType.Fast:
                    if (Ball.Instance.pSpeed < 2f)
                    {
                        Ball.Instance.pSpeed += 0.2f;
                    }

                    break;
            }

            Destroy (gameObject);
        }
    }
}