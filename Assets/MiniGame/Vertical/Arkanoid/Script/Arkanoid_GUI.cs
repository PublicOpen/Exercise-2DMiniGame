﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.Arkanoid
{
    public class Arkanoid_GUI : MonoBehaviour
    {
        public static Arkanoid_GUI Instance;
        private GameStatus fStatus;
        public GameStatus pStatus
        {
            get
            {
                return fStatus;
            }
            set
            {
                fStatus = value;

                switch (value)
                {
                    case GameStatus.SelectedLevel:
                        fPanel_SelectedLevel.gameObject.SetActive (true);

                        break;
                    case GameStatus.Playing:
                        Time.timeScale = 1;

                        break;
                    case GameStatus.Pause:
                        Time.timeScale = 0;

                        break;
                    case GameStatus.Win:
                        if (PlayerPrefs.GetInt ("OpenLevel") == pLevel)
                        {
                            PlayerPrefs.SetInt ("OpenLevel", pLevel + 1);
                        }
                        fPanel_Result.Show (pStatus);

                        break;
                    case GameStatus.Lose:
                        fPanel_Result.Show (pStatus);

                        break;
                }
            }
        }

        [Header("Header")]
        public Text fTxt_Level;
        public int pLevel
        {
            get
            {
                return Convert.ToInt32 (fTxt_Level.text);
            }
            set
            {
                fTxt_Level.text = value.ToString ();
            }
        }
        public Text fTxt_Life;
        public int pLife
        {
            get
            {
                return Convert.ToInt32 (fTxt_Life.text);
            }
            set
            {
                fTxt_Life.text = value.ToString ();

                if (value == 0)
                {
                    pStatus = GameStatus.Lose;
                }
                else
                {
                    Board.Instance.LaunchReady ();
                }
            }
        }

        [Header("Panel")]
        public SelectedLevelPanel fPanel_SelectedLevel;
        public GameObject fPanel_Pause;
        public ResultPanel fPanel_Result;

        void Awake ()
        {
            Instance = this;
        }

        void Start ()
        {
            if (!PlayerPrefs.HasKey ("OpenLevel"))
            {
                PlayerPrefs.SetInt ("OpenLevel", 1);
            }

            pStatus = GameStatus.SelectedLevel;
        }

        void Update ()
        {
            if (Input.GetKeyDown (KeyCode.Escape))
            {
                if (pStatus == GameStatus.Playing)
                {
                    fPanel_Pause.SetActive (true);
                    pStatus = GameStatus.Pause;
                }
                else if (pStatus == GameStatus.Pause)
                {
                    fPanel_Pause.SetActive (false);
                    pStatus = GameStatus.Playing;
                }
            }
        }

        public void Reset_Game (int lv)
        {
            pLevel = lv;
            pLife = 3;
            ArkanoidSystem.Instance.Setup_GameStart (lv);

            pStatus = GameStatus.Playing;
        }
    }
}