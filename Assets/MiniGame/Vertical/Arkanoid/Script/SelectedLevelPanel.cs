﻿using UnityEngine;

namespace MapleStar.Arkanoid
{
    public class SelectedLevelPanel : MonoBehaviour
    {
        public static SelectedLevelPanel Instance;

        public GameObject fPrefab_Btn_LV;

        void Awake ()
        {
            Instance = this;
        }

        void Start ()
        {
            for (int i = 0; i < ArkanoidSystem.Instance.fLevelData.Count; i++)
            {
                GameObject obj = Instantiate (fPrefab_Btn_LV, transform);
                obj.GetComponent<Btn_LV> ().fTxt_LV.text = (i + 1).ToString ();
                obj.SetActive (true);
            }
        }
    }
}