﻿using UnityEngine;

namespace MapleStar.Arkanoid
{
    public class Board : MonoBehaviour
    {
        public static Board Instance;

        private float pPosX
        {
            get
            {
                return transform.localPosition.x;
            }
            set
            {
                float board_edge_value = (ArkanoidSystem.Instance.fGameArea.sizeDelta.x - GetComponent<RectTransform> ().sizeDelta.x) / 2;

                if (value > board_edge_value)
                {
                    transform.localPosition = new Vector2 (board_edge_value, transform.localPosition.y);
                }
                else if (value < -board_edge_value)
                {
                    transform.localPosition = new Vector2 (-board_edge_value, transform.localPosition.y);
                }
                else
                {
                    transform.localPosition = new Vector2 (value, transform.localPosition.y);
                }
            }
        }
        private const float fMoveSpeed = 500f;

        public GameObject fPrefab_Ball;

        void Awake ()
        {
            Instance = this;
        }

        void Update ()
        {
            if (Arkanoid_GUI.Instance.pStatus == GameStatus.Playing)
            {
                #if UNITY_EDITOR
                if (Input.GetKey (KeyCode.LeftArrow))
                {
                    pPosX -= Time.deltaTime * fMoveSpeed;
                }
                if (Input.GetKey (KeyCode.RightArrow))
                {
                    pPosX += Time.deltaTime * fMoveSpeed;
                }
                #elif UNITY_ANDROID || UNITY_IOS

                #endif
            }
        }

        public void LaunchReady ()
        {
            pPosX = 0;
            GetComponent<RectTransform> ().sizeDelta = new Vector2 (250, 50);

            if (transform.GetComponentInChildren<Ball> () == null)
            {
                GameObject ball = Instantiate (fPrefab_Ball, transform);
                ball.transform.localPosition = new Vector2 (0, ball.GetComponent<RectTransform> ().sizeDelta.y);
            }
        }
    }
}