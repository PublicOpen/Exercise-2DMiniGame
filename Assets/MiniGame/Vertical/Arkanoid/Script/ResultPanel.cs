﻿using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.Arkanoid
{
    public class ResultPanel : MonoBehaviour
    {
        [Header ("Win Windows")]
        public GameObject fWin_Windows;
        public Button fBtn_WW_Next_Level;
        public Button fBtn_WW_Exit;

        [Header ("Lose Windows")]
        public GameObject fLose_Windows;
        public Button fBtn_LW_Restart;
        public Button fBtn_LW_Exit;

        void Start ()
        {
            fBtn_WW_Next_Level.onClick.AddListener (OnClick_Next_Level);
            fBtn_WW_Exit.onClick.AddListener (OnClick_Exit);

            fBtn_LW_Restart.onClick.AddListener (OnClick_Restart);
            fBtn_LW_Exit.onClick.AddListener (OnClick_Exit);
        }

        void OnDisable ()
        {
            fWin_Windows.SetActive (false);
            fLose_Windows.SetActive (false);
        }

        public void Show (GameStatus status)
        {
            gameObject.SetActive (true);

            switch (status)
            {
                case GameStatus.Win:
                    if (Arkanoid_GUI.Instance.pLevel == ArkanoidSystem.Instance.fLevelData.Count)
                    {
                        fBtn_WW_Next_Level.gameObject.SetActive (false);
                    }
                    else
                    {
                        fBtn_WW_Next_Level.gameObject.SetActive (true);
                    }

                    fWin_Windows.SetActive (true);

                    break;
                case GameStatus.Lose:
                    fLose_Windows.SetActive (true);

                    break;
            }
        }

        #region UI
        private void OnClick_Next_Level ()
        {
            gameObject.SetActive (false);
            Arkanoid_GUI.Instance.Reset_Game (Arkanoid_GUI.Instance.pLevel + 1);
        }

        private void OnClick_Restart ()
        {
            gameObject.SetActive (false);
            Arkanoid_GUI.Instance.Reset_Game (Arkanoid_GUI.Instance.pLevel);
        }

        private void OnClick_Exit ()
        {
            Application.Quit ();
        }
        #endregion
    }
}