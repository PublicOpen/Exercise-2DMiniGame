﻿using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.GordonRamsay
{
    public class ResultPanel : MonoBehaviour
    {
        public Button fBtn_Restart;
        public Button fBtn_Exit;

        void Start () 
        {
            fBtn_Restart.onClick.AddListener (OnClick_Restart);
            fBtn_Exit.onClick.AddListener (OnClick_Exit);
        }

        #region UI
        private void OnClick_Restart ()
        {
            gameObject.SetActive (false);
            WitchBoiler_GUI.Instance.pStatus = GameStatus.CountDown;
        }

        private void OnClick_Exit ()
        {
            Application.Quit ();
        }
        #endregion
    }
}