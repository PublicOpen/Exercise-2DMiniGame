﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MapleStar.GordonRamsay
{
    public class FoodIngredient : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
    {
        private int fUnit;

        public Rigidbody2D fRB2D_Self;
        private Vector3 fDirection;
        private float fThrust;
        private const float fMin_Thrust = 130.0f;
        private const float fMax_Thrust = 160.0f;

        public int fValue;

        private Vector2 fPointer_ScreenPos; //紀錄手指觸碰位置
        private bool fIS_Selected = false;

        void Start ()
        {
            fUnit = Convert.ToInt32 (GetComponent<RectTransform> ().sizeDelta.x);

            /* 決定食材起始位置「左／右」？與飛行方向 */
            switch (UnityEngine.Random.Range (0, 2))
            {
                case 0: // Left
                    transform.localPosition = new Vector3 ((transform.parent.GetComponent<RectTransform> ().sizeDelta.x + fUnit) / 2, UnityEngine.Random.Range (-fUnit, fUnit) / 2);
                    fDirection = -transform.right;

                    break;
                case 1: // Right
                    transform.localPosition = new Vector3 (-(transform.parent.GetComponent<RectTransform> ().sizeDelta.x + fUnit) / 2, UnityEngine.Random.Range (-fUnit, fUnit) / 2);
                    fDirection = transform.right;

                    break;
            }

            /* 隨機力道 */
            fThrust = UnityEngine.Random.Range (fMin_Thrust, fMax_Thrust);

            /* 施力飛行 */
            fRB2D_Self.AddForce (transform.up * fThrust / 2 + fDirection * fThrust);
        }

        #region UI
        public void OnPointerDown (PointerEventData data)
        {
            fIS_Selected = true;

            fRB2D_Self.velocity = Vector3.zero;
            fRB2D_Self.angularVelocity = 0;
            fRB2D_Self.gravityScale = 0;

            #if UNITY_EDITOR
            fPointer_ScreenPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
            #elif UNITY_ANDROID || UNITY_IOS
            if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Began)
            {
                fPointer_ScreenPos = Input.touches[0].position;
            }
            #endif
        }

        public void OnDrag (PointerEventData data)
        {
            if (fIS_Selected == true)
            {
                #if UNITY_EDITOR
                transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0));
                #elif UNITY_ANDROID || UNITY_IOS
                if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Moved)
                {
                    transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (Input.touches[0].position.x, Input.touches[0].position.y, 0));
                }
                #endif

                transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y, 0);
            }
        }

        public void OnPointerUp (PointerEventData data)
        {
            if (fIS_Selected == true)
            {
                fRB2D_Self.gravityScale = 0.2f;
                Vector2 pos = new Vector2 ();

                #if UNITY_EDITOR
                pos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
                #elif UNITY_ANDROID || UNITY_IOS
                if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
                {
                    pos = Input.touches[0].position;
                }
                #endif

                fRB2D_Self.AddForce ((pos - fPointer_ScreenPos).normalized * 50.0f);
                fIS_Selected = false;
            }
        }
        #endregion

        void OnTriggerEnter2D (Collider2D other)
        {
            if (other.transform == WitchBoilerSystem.Instance.fCuisine_Region)
            {
                WitchBoiler_GUI.Instance.pProgress += fValue;

                ParticleSystem particle_system = Instantiate (WitchBoilerSystem.Instance.fFallingWater_Effect, WitchBoilerSystem.Instance.fFood_Ingredient_Region);
                particle_system.transform.localPosition = transform.localPosition;

                Destroy (gameObject);
            }
        }

        void OnTriggerExit2D (Collider2D other)
        {
            if (other.transform == WitchBoilerSystem.Instance.fKitchen_Waste_Region)
            {
                Destroy (gameObject);
            }
        }
    }
}