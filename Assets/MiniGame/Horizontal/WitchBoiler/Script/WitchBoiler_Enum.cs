﻿namespace MapleStar.GordonRamsay
{
    public enum GameStatus
    {
        CountDown,
        Playing,
        Pause,
        Result
    }
}