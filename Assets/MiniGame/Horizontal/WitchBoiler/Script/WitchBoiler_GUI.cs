﻿using MapleStar.Timer;
using MapleStar.Tween;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.GordonRamsay
{
    public class WitchBoiler_GUI : MonoBehaviour
    {
        public static WitchBoiler_GUI Instance;
        private GameStatus fStatus;
        public GameStatus pStatus
        {
            get
            {
                return fStatus;
            }
            set
            {
                fStatus = value;

                switch (value)
                {
                    case GameStatus.CountDown:
                        Time.timeScale = 1;

                        Reset_Game ();

                        fPanel_CountDown.gameObject.SetActive (true);
                        fPanel_CountDown.Play (0, (TweenStatus s) => {
                            WitchBoilerSystem.Instance.Begin_Increase ();
                            pStatus = GameStatus.Playing;
                        });

                        break;
                    case GameStatus.Playing:
                        Time.timeScale = 1;

                        fTime.Play ((TimerFinishStatus s) => {
                            if (s == TimerFinishStatus.Normal)
                            {
                                pStatus = GameStatus.Result;
                            }
                        });

                        break;
                    case GameStatus.Pause:
                        Time.timeScale = 0;

                        fTime.Pause ();

                        break;
                    case GameStatus.Result:
                        End_Game ();
                        fPanel_Result.gameObject.SetActive (true);

                        break;
                }
            }
        }

        [Header ("Visual UI")]
        public Slider fSlider_Time_Bar;
        public TimerCountDown fTime;
        public Slider fSlider_Progress_Bar;
        public Text fTxt_Progress;
        private int fProgress = 0;
        public int pProgress
        {
            get
            {
                return fProgress;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }

                fProgress = value;
                fSlider_Progress_Bar.value = fProgress;

                if (fProgress >= 0)
                {
                    fTxt_Progress.text = string.Format ("{0}<size=30>{1}</size>", Convert.ToInt16 (fProgress / fSlider_Progress_Bar.maxValue * 100), "%");
                }
                else
                {
                    fTxt_Progress.text = "0<size=30>%</size>";
                }
            }
        }

        [Header ("Panel")]
        public CountDownPanel fPanel_CountDown;
        public GameObject fPanel_Pause;
        public ResultPanel fPanel_Result;

        void Awake ()
        {
            Instance = this;
        }

        void Start ()
        {
            fTime.GetTime ((double d) => {
                fSlider_Time_Bar.value = Convert.ToSingle (d);
            });
            fTime.Setup (new TimeSpan (0, 0, 60));

            fSlider_Progress_Bar.maxValue = 30;

            pStatus = GameStatus.CountDown;
        }

        void Update ()
        {
            /* 鍵盤事件 */
            if (Input.GetKeyDown (KeyCode.Escape))
            {
                if (pStatus == GameStatus.Playing)
                {
                    fPanel_Pause.SetActive (true);
                    pStatus = GameStatus.Pause;
                }
                else if (pStatus == GameStatus.Pause)
                {
                    fPanel_Pause.SetActive (false);
                    pStatus = GameStatus.Playing;
                }
            }
        }

        private void Reset_Game ()
        {
            fSlider_Time_Bar.value = fSlider_Time_Bar.maxValue;
            pProgress = 0;
        }

        public void End_Game ()
        {
            fTime.Stop ();

            WitchBoilerSystem.Instance.Stop_Increase ();
            WitchBoilerSystem.Instance.Clearance ();
        }
    }
}