﻿using System.Collections;
using UnityEngine;

namespace MapleStar.GordonRamsay
{
    public class WitchBoilerSystem : MonoBehaviour
    {
        public static WitchBoilerSystem Instance;

        [Header ("Region")]
        public Transform fFood_Ingredient_Region;
        public Transform fCuisine_Region;
        public Transform fKitchen_Waste_Region;

        [Header ("Food Ingredient")]
        public GameObject[] fPrefab_Food_Ingredient;
        public ParticleSystem fFallingWater_Effect;

        private float fFrequency = 1f;
        private Coroutine fCoroutine_Increasing;

        void Awake ()
        {
            Instance = this;
        }

        public void Begin_Increase ()
        {
            fCoroutine_Increasing = StartCoroutine (_Increasing ());
        }

        private IEnumerator _Increasing ()
        {
            while (true)
            {
                Instantiate (fPrefab_Food_Ingredient[UnityEngine.Random.Range (0, fPrefab_Food_Ingredient.Length)], fFood_Ingredient_Region);

                yield return new WaitForSeconds (fFrequency);
            }
        }

        public void Stop_Increase ()
        {
            StopCoroutine (fCoroutine_Increasing);
        }

        public void Clearance ()
        {
            TransformEx.ChildClear (fFood_Ingredient_Region.transform);
        }
    }
}