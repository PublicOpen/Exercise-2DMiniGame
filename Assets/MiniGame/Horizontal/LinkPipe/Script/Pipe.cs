﻿using MapleStar.Tween;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MapleStar.LinkPipe
{
    public class Pipe : TweenScale, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public static GameObject fSelected_Obj;

        private Vector3 fOrigin_Pos;
        private Transform fOrigin_Parent;

        [Header ("Pipe")]
        public PipeType fType;

        #region UI
        public void OnBeginDrag (PointerEventData data)
        {
            fSelected_Obj = gameObject;
            fOrigin_Pos = transform.localPosition;
            fOrigin_Parent = transform.parent;
            GetComponent<Image> ().raycastTarget = false;
        }

        public void OnDrag (PointerEventData data)
        {
            #if UNITY_EDITOR
            transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0));
            #elif UNITY_ANDROID || UNITY_IOS
            transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (Input.touches[0].position.x, Input.touches[0].position.y, 0));
            #endif

            transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y, 0);
        }

        public void OnEndDrag (PointerEventData data)
        {
            fSelected_Obj = null;
            GetComponent<Image> ().raycastTarget = true;

            if (transform.parent == fOrigin_Parent)
            {
                transform.localPosition = fOrigin_Pos;
            }
            else
            {
                GetComponent<Image> ().raycastTarget = false;
            }
        }
        #endregion
    }
}