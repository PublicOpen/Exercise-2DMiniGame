﻿namespace MapleStar.LinkPipe
{
    public enum GameStatus
    {
        CountDown,
        Playing,
        Pause,
        Win,
        Lose
    }

    public enum PipeType : int
    {
        Crossroads = 0, // +
        Up_Right = 1,   // ⌜
        Right_Down = 2, // ⌝
        Down_Left = 3,  // ⌟
        Left_Up = 4,    // ⌞
        Column = 5,     // |
        Row = 6         // -
    }

    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }
}