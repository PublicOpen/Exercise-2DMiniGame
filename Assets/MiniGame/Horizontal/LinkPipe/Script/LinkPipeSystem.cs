﻿using MapleStar.Tween;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.LinkPipe
{
    public class LinkPipeSystem : MonoBehaviour
    {
        public static LinkPipeSystem Instance;
        public int fLV;
        public LinkPipeSetting fSettingFile;

        [Header ("Game Pool Object")]
        public const int fUnit = 170;
        public GameObject fPrefab_SpliceSlot;
        public GameObject fPrefab_Obstacle;
        public GameObject fPrefab_MaterialSlot;
        public GameObject[] fPrefab_Pipe;

        [Header ("Splice Region")]
        public RectTransform fSpliceRegion;
        public GridLayoutGroup fGridLayout_SpliceRegion;
        /// <summary>
        /// X
        /// </summary>
        private int fTotal_Column;
        /// <summary>
        /// Y
        /// </summary>
        private int fTotal_Row;

        [Header ("Material Region")]
        public RectTransform fMaterialRegion;
        public GridLayoutGroup fGridLayout_MaterialRegion;

        [Header ("Record Use")]
        public Dictionary<string, SpliceSlot> fSpliceRegion_Slot = new Dictionary<string, SpliceSlot> ();

        void Awake ()
        {
            Instance = this;
        }

        void Start ()
        {
            fLV = 0;
            fTotal_Column = Convert.ToInt32 (fSettingFile.fXY.x);
            fTotal_Row = Convert.ToInt32 (fSettingFile.fXY.y);

            /* 設定各 GridLayoutGroup 的 cellSize */
            fGridLayout_SpliceRegion.cellSize = new Vector2 (fUnit, fUnit);
            fGridLayout_MaterialRegion.cellSize = new Vector2 (fUnit, fUnit);
            fPrefab_SpliceSlot.GetComponent<GridLayoutGroup> ().cellSize = new Vector2 (fUnit, fUnit);
            fPrefab_MaterialSlot.GetComponent<GridLayoutGroup> ().cellSize = new Vector2 (fUnit, fUnit);

            Setup_SpliceRegion ();
        }

        public void SetUp_GameStart ()
        {
            /* 清空上一輪拼接區的水管 */
            foreach (string key in fSpliceRegion_Slot.Keys)
            {
                Pipe pipe = fSpliceRegion_Slot[key].GetComponentInChildren<Pipe> ();

                if (pipe != null)
                {
                    Destroy (pipe.gameObject);
                }
            }

            /* 起始之路、工人位置設定 */
            Worker.Instance.Setup_Coordinate (Convert.ToInt32 (fSettingFile.fCharacterPos[fLV].fWorkerPos.x), Convert.ToInt32 (fSettingFile.fCharacterPos[fLV].fWorkerPos.y));
            Transform begin_slot = fSpliceRegion_Slot[string.Format ("{0},{1}", Worker.Instance.fColumn, Worker.Instance.fRow)].transform;
            GameObject begin_pipe = Instantiate (fPrefab_Pipe[6], begin_slot);
            begin_pipe.transform.localScale = Vector3.one;
            Worker.Instance.transform.localPosition = new Vector3 (begin_slot.localPosition.x - fUnit / 2, begin_slot.localPosition.y, 0);

            /* 終點之路、公主位置設定 */
            Princess.Instance.Setup_Coordinate (Convert.ToInt32 (fSettingFile.fCharacterPos[fLV].fPrincessPos.x), Convert.ToInt32 (fSettingFile.fCharacterPos[fLV].fPrincessPos.y));
            Transform end_slot = fSpliceRegion_Slot[string.Format ("{0},{1}", Princess.Instance.fColumn, Princess.Instance.fRow)].transform;
            GameObject end_pipe = Instantiate (fPrefab_Pipe[6], end_slot);
            end_pipe.transform.localScale = Vector3.one;
            Princess.Instance.transform.localPosition = new Vector3 (end_slot.localPosition.x + fUnit / 2, end_slot.localPosition.y, 0);

            /* 生成水管原料 */
            Setup_MaterialRegion ();
        }

        #region Splice Region 拼接區
        private void Setup_SpliceRegion ()
        {
            StartCoroutine (_Setup_SpliceRegion ());
        }

        private IEnumerator _Setup_SpliceRegion ()
        {
            for (int i = 0; i < fTotal_Column; i++)
            {
                for (int j = 0; j < fTotal_Row; j++)
                {
                    GameObject obj = Instantiate (fPrefab_SpliceSlot, fSpliceRegion);
                    SpliceSlot slot = obj.GetComponent<SpliceSlot> ();
                    slot.Setup_Coordinate (i, j);

                    if (fSettingFile.fSpliceRegion[fLV].fX_Column[i].fY_Row[j] == false)
                    {
                        Instantiate (fPrefab_Obstacle, slot.transform); // 生成拼接區障礙物
                    }

                    fSpliceRegion_Slot.Add (string.Format ("{0},{1}", i, j), slot);

                    #if UNITY_EDITOR
                    obj.name = string.Format ("{0},{1}", i, j);
                    #endif

                    if (i == 0 || i == fTotal_Column - 1)
                    {
                        obj.GetComponent<Image> ().raycastTarget = false;
                        obj.GetComponent<Image> ().color = Color.clear;
                    }
                }
            }

            yield return new WaitForEndOfFrame ();

            fGridLayout_SpliceRegion.enabled = false;
            Princess.Instance.transform.SetAsLastSibling ();
            Worker.Instance.transform.SetAsLastSibling ();
        }
        #endregion

        #region Material Region 原料區
        private void Setup_MaterialRegion ()
        {
            StartCoroutine (_Setup_MaterialRegion ());
        }

        private IEnumerator _Setup_MaterialRegion ()
        {
            TransformEx.ChildClear (fMaterialRegion);
            fGridLayout_MaterialRegion.enabled = true;

            PipeCombination pipe_combination = fSettingFile.fMaterialRegion[fLV].fPipeCombination[UnityEngine.Random.Range (0, fSettingFile.fMaterialRegion[fLV].fPipeCombination.Length)];

            for (int i = 0; i < pipe_combination.fPipe.Length; i++)
            {
                GameObject slot = Instantiate (fPrefab_MaterialSlot, fMaterialRegion);
                GameObject pipe = Instantiate (fPrefab_Pipe[pipe_combination.fPipe[i]], slot.transform);

                if (i < 4)
                {
                    pipe.transform.localScale = Vector3.one;
                }
            }

            yield return new WaitForEndOfFrame ();

            OpenFirstMaterial ();
            fGridLayout_MaterialRegion.enabled = false;
        }

        public void SupplyMaterial ()
        {
            DestroyImmediate (fMaterialRegion.transform.GetChild (0).gameObject);

            MaterialSlot[] MaterialSlots = fMaterialRegion.GetComponentsInChildren<MaterialSlot> ();

            foreach (MaterialSlot material_slot in MaterialSlots)
            {
                material_slot.Move ();
            }

            fMaterialRegion.transform.GetChild (3).GetComponentInChildren<Pipe> ().Play (0, (TweenStatus s) => {
                OpenFirstMaterial ();
            });
        }

        private void OpenFirstMaterial ()
        {
            fMaterialRegion.transform.GetChild (0).GetComponent<Image> ().color = Color.white; // 第一格插槽顏色開啟
            fMaterialRegion.transform.GetChild (0).GetComponentInChildren<Pipe> ().GetComponent<Image> ().raycastTarget = true; // 第一格水管的 raycastTarget 要打開
        }
        #endregion
    }
}