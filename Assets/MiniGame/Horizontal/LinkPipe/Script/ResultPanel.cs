﻿using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.LinkPipe
{
    public class ResultPanel : MonoBehaviour
    {
        [Header ("Win Windows")]
        public GameObject fWin_Windows;
        public Button fBtn_WW_Restart;
        public Button fBtn_WW_Exit;

        [Header ("Lose Windows")]
        public GameObject fLose_Windows;
        public Button fBtn_LW_Restart;
        public Button fBtn_LW_Exit;

        void Start ()
        {
            fBtn_WW_Restart.onClick.AddListener (OnClick_Restart);
            fBtn_WW_Exit.onClick.AddListener (OnClick_Exit);

            fBtn_LW_Restart.onClick.AddListener (OnClick_Restart);
            fBtn_LW_Exit.onClick.AddListener (OnClick_Exit);
        }

        void OnDisable ()
        {
            fWin_Windows.SetActive (false);
            fLose_Windows.SetActive (false);
        }

        public void Show (GameStatus status)
        {
            gameObject.SetActive (true);

            switch (status)
            {
                case GameStatus.Win:
                    fWin_Windows.SetActive (true);

                    break;
                case GameStatus.Lose:
                    fLose_Windows.SetActive (true);

                    break;
            }
        }

        #region UI
        private void OnClick_Restart ()
        {
            gameObject.SetActive (false);
            LinkPipe_GUI.Instance.pStatus = GameStatus.CountDown;
        }

        private void OnClick_Exit ()
        {
            Application.Quit ();
        }
        #endregion
    }
}