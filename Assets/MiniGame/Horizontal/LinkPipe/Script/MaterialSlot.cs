﻿using MapleStar.Tween;
using System;
using System.Collections;
using UnityEngine;

namespace MapleStar.LinkPipe
{
    public class MaterialSlot : TweenPosition
    {
        private Vector3 fFrom;
        private float fMoveDistance;

        void Start ()
        {
            fMoveDistance = LinkPipeSystem.fUnit + LinkPipeSystem.Instance.fGridLayout_MaterialRegion.spacing.x;
        }

        public void Move ()
        {
            fFrom = transform.localPosition;
            Play (0, (TweenStatus s) => {});
        }

        protected override IEnumerator _Play (Action<TweenStatus> callback)
        {
            while (true)
            {
                fStatus = TweenStatus.Play;

                while (fCurrent_Time < fTotal_Time && fStatus == TweenStatus.Play)
                {
                    fCurrent_Time += Time.deltaTime * fSpeed;

                    float evaluate_x = fXYZ_AnimationCurve.fX.Evaluate (fCurrent_Time / fTotal_Time);

                    transform.localPosition = new Vector3 (fFrom.x + fMoveDistance * evaluate_x, transform.localPosition.y, 0);

                    yield return null;
                }

                if (fCurrent_Time >= fTotal_Time)
                {
                    fStatus = TweenStatus.Stop;
                }

                callback (fStatus);

                if (fLoop == false)
                {
                    break;
                }
                else
                {
                    fCurrent_Time = 0;
                }
            }
        }
    }
}