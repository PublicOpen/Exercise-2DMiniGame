﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace MapleStar.LinkPipe
{
    public class SpliceSlot : MonoBehaviour, IDropHandler
    {
        public int fColumn;
        public int fRow;

        public void Setup_Coordinate (int column, int row)
        {
            fColumn = column;
            fRow = row;
        }

        #region UI
        public void OnDrop (PointerEventData data)
        {
            if (transform.childCount == 0 && Pipe.fSelected_Obj != null)
            {
                Pipe.fSelected_Obj.transform.SetParent (transform);
                LinkPipeSystem.Instance.SupplyMaterial ();
            }
        }
        #endregion
    }
}