﻿using UnityEngine;

namespace MapleStar.LinkPipe
{
    public class Princess : MonoBehaviour
    {
        public static Princess Instance;

        public int fColumn;
        public int fRow;

        void Awake ()
        {
            Instance = this;
        }

        public void Setup_Coordinate (int column, int row)
        {
            fColumn = column;
            fRow = row;
        }
    }
}