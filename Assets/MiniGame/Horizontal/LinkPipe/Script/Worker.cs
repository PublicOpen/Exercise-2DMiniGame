﻿using MapleStar.Tween;
using System;
using System.Collections;
using UnityEngine;

namespace MapleStar.LinkPipe
{
    public class Worker : TweenPosition
    {
        public static Worker Instance;

        private Vector3 fFrom;
        private Vector3 fTo;

        [Header ("Worker")]
        public int fColumn;
        public int fRow;

        void Awake ()
        {
            Instance = this;
        }

        public void Setup_Coordinate (int column, int row)
        {
            fColumn = column;
            fRow = row;
        }

        public void Move (Direction direction)
        {
            try
            {
                fFrom = transform.localPosition;

                switch (LinkPipeSystem.Instance.fSpliceRegion_Slot[string.Format ("{0},{1}", fColumn, fRow)].GetComponentInChildren<Pipe> ().fType)
                {
                    case PipeType.Crossroads: // +
                        int r = UnityEngine.Random.Range (1, 4);

                        if (direction == Direction.Up)
                        {
                            if (r == 1)
                            {
                                goto case PipeType.Column;
                            }
                            else if (r == 2)
                            {
                                goto case PipeType.Right_Down;
                            }
                            else if (r == 3)
                            {
                                goto case PipeType.Up_Right;
                            }
                        }
                        else if (direction == Direction.Down)
                        {
                            if (r == 1)
                            {
                                goto case PipeType.Column;
                            }
                            else if (r == 2)
                            {
                                goto case PipeType.Down_Left;
                            }
                            else if (r == 3)
                            {
                                goto case PipeType.Left_Up;
                            }
                        }
                        else if (direction == Direction.Left)
                        {
                            if (r == 1)
                            {
                                goto case PipeType.Row;
                            }
                            else if (r == 2)
                            {
                                goto case PipeType.Left_Up;
                            }
                            else if (r == 3)
                            {
                                goto case PipeType.Up_Right;
                            }
                        }
                        else if (direction == Direction.Right)
                        {
                            if (r == 1)
                            {
                                goto case PipeType.Row;
                            }
                            else if (r == 2)
                            {
                                goto case PipeType.Down_Left;
                            }
                            else if (r == 3)
                            {
                                goto case PipeType.Right_Down;
                            }
                        }

                        break;
                    case PipeType.Up_Right: // ⌜
                        if (direction == Direction.Up)
                        {
                            fTo = new Vector3 (transform.localPosition.x + LinkPipeSystem.fUnit, transform.localPosition.y + LinkPipeSystem.fUnit, 0);
                            Play (3, (TweenStatus s) => {
                                if (LinkPipe_GUI.Instance.pStatus == GameStatus.Playing)
                                {
                                    if (Judge () == true)
                                    {
                                        LinkPipe_GUI.Instance.pStatus = GameStatus.Win;
                                    }
                                    else
                                    {
                                        fColumn++;
                                        Move (Direction.Right);
                                    }
                                }
                            });
                        }
                        else if (direction == Direction.Left)
                        {
                            fTo = new Vector3 (transform.localPosition.x - LinkPipeSystem.fUnit, transform.localPosition.y - LinkPipeSystem.fUnit, 0);
                            Play (1, (TweenStatus s) => {
                                if (LinkPipe_GUI.Instance.pStatus == GameStatus.Playing)
                                {
                                    if (Judge () == true)
                                    {
                                        LinkPipe_GUI.Instance.pStatus = GameStatus.Win;
                                    }
                                    else
                                    {
                                        fRow--;
                                        Move (Direction.Down);
                                    }
                                }
                            });
                        }

                        break;
                    case PipeType.Right_Down: // ⌝
                        if (direction == Direction.Up)
                        {
                            fTo = new Vector3 (transform.localPosition.x - LinkPipeSystem.fUnit, transform.localPosition.y + LinkPipeSystem.fUnit, 0);
                            Play (3, (TweenStatus s) => {
                                if (LinkPipe_GUI.Instance.pStatus == GameStatus.Playing)
                                {
                                    if (Judge () == true)
                                    {
                                        LinkPipe_GUI.Instance.pStatus = GameStatus.Win;
                                    }
                                    else
                                    {
                                        fColumn--;
                                        Move (Direction.Left);
                                    }
                                }
                            });
                        }
                        else if (direction == Direction.Right)
                        {
                            fTo = new Vector3 (transform.localPosition.x + LinkPipeSystem.fUnit, transform.localPosition.y - LinkPipeSystem.fUnit, 0);
                            Play (1, (TweenStatus s) => {
                                if (LinkPipe_GUI.Instance.pStatus == GameStatus.Playing)
                                {
                                    if (Judge () == true)
                                    {
                                        LinkPipe_GUI.Instance.pStatus = GameStatus.Win;
                                    }
                                    else
                                    {
                                        fRow--;
                                        Move (Direction.Down);
                                    }
                                }
                            });
                        }

                        break;
                    case PipeType.Down_Left: // ⌟
                        if (direction == Direction.Down)
                        {
                            fTo = new Vector3 (transform.localPosition.x - LinkPipeSystem.fUnit, transform.localPosition.y - LinkPipeSystem.fUnit, 0);
                            Play (3, (TweenStatus s) => {
                                if (LinkPipe_GUI.Instance.pStatus == GameStatus.Playing)
                                {
                                    if (Judge () == true)
                                    {
                                        LinkPipe_GUI.Instance.pStatus = GameStatus.Win;
                                    }
                                    else
                                    {
                                        fColumn--;
                                        Move (Direction.Left);
                                    }
                                }
                            });
                        }
                        else if (direction == Direction.Right)
                        {
                            fTo = new Vector3 (transform.localPosition.x + LinkPipeSystem.fUnit, transform.localPosition.y + LinkPipeSystem.fUnit, 0);
                            Play (1, (TweenStatus s) => {
                                if (LinkPipe_GUI.Instance.pStatus == GameStatus.Playing)
                                {
                                    if (Judge () == true)
                                    {
                                        LinkPipe_GUI.Instance.pStatus = GameStatus.Win;
                                    }
                                    else
                                    {
                                        fRow++;
                                        Move (Direction.Up);
                                    }
                                }
                            });
                        }

                        break;
                    case PipeType.Left_Up: // ⌞
                        if (direction == Direction.Down)
                        {
                            fTo = new Vector3 (transform.localPosition.x + LinkPipeSystem.fUnit, transform.localPosition.y - LinkPipeSystem.fUnit, 0);
                            Play (3, (TweenStatus s) => {
                                if (LinkPipe_GUI.Instance.pStatus == GameStatus.Playing)
                                {
                                    if (Judge () == true)
                                    {
                                        LinkPipe_GUI.Instance.pStatus = GameStatus.Win;
                                    }
                                    else
                                    {
                                        fColumn++;
                                        Move (Direction.Right);
                                    }
                                }
                            });
                        }
                        else if (direction == Direction.Left)
                        {
                            fTo = new Vector3 (transform.localPosition.x - LinkPipeSystem.fUnit, transform.localPosition.y + LinkPipeSystem.fUnit, 0);
                            Play (1, (TweenStatus s) => {
                                if (LinkPipe_GUI.Instance.pStatus == GameStatus.Playing)
                                {
                                    if (Judge () == true)
                                    {
                                        LinkPipe_GUI.Instance.pStatus = GameStatus.Win;
                                    }
                                    else
                                    {
                                        fRow++;
                                        Move (Direction.Up);
                                    }
                                }
                            });
                        }

                        break;
                    case PipeType.Column: // |
                        if (direction == Direction.Up)
                        {
                            fTo = new Vector3 (transform.localPosition.x, transform.localPosition.y + LinkPipeSystem.fUnit, 0);
                            Play (2, (TweenStatus s) => {
                                if (LinkPipe_GUI.Instance.pStatus == GameStatus.Playing)
                                {
                                    if (Judge () == true)
                                    {
                                        LinkPipe_GUI.Instance.pStatus = GameStatus.Win;
                                    }
                                    else
                                    {
                                        fRow++;
                                        Move (Direction.Up);
                                    }
                                }
                            });
                        }
                        else if (direction == Direction.Down)
                        {
                            fTo = new Vector3 (transform.localPosition.x, transform.localPosition.y - LinkPipeSystem.fUnit, 0);
                            Play (2, (TweenStatus s) => {
                                if (LinkPipe_GUI.Instance.pStatus == GameStatus.Playing)
                                {
                                    if (Judge () == true)
                                    {
                                        LinkPipe_GUI.Instance.pStatus = GameStatus.Win;
                                    }
                                    else
                                    {
                                        fRow--;
                                        Move (Direction.Down);
                                    }
                                }
                            });
                        }

                        break;
                    case PipeType.Row: // -
                        if (direction == Direction.Left)
                        {
                            fTo = new Vector3 (transform.localPosition.x - LinkPipeSystem.fUnit, transform.localPosition.y, 0);
                            Play (0, (TweenStatus s) => {
                                if (LinkPipe_GUI.Instance.pStatus == GameStatus.Playing)
                                {
                                    if (Judge () == true)
                                    {
                                        LinkPipe_GUI.Instance.pStatus = GameStatus.Win;
                                    }
                                    else
                                    {
                                        fColumn--;
                                        Move (Direction.Left);
                                    }
                                }
                            });
                        }
                        else if (direction == Direction.Right)
                        {
                            fTo = new Vector3 (transform.localPosition.x + LinkPipeSystem.fUnit, transform.localPosition.y, 0);
                            Play (0, (TweenStatus s) => {
                                if (LinkPipe_GUI.Instance.pStatus == GameStatus.Playing)
                                {
                                    if (Judge () == true)
                                    {
                                        LinkPipe_GUI.Instance.pStatus = GameStatus.Win;
                                    }
                                    else
                                    {
                                        fColumn++;
                                        Move (Direction.Right);
                                    }
                                }
                            });
                        }

                        break;
                }
            }
            catch
            {
                LinkPipe_GUI.Instance.pStatus = GameStatus.Lose;
            }
        }

        protected override IEnumerator _Play (Action<TweenStatus> callback)
        {
            while (true)
            {
                fStatus = TweenStatus.Play;

                while (fCurrent_Time < fTotal_Time && fStatus == TweenStatus.Play)
                {
                    fCurrent_Time += Time.deltaTime * fSpeed;

                    float evaluate_x = fXYZ_AnimationCurve.fX.Evaluate (fCurrent_Time / fTotal_Time);
                    float evaluate_y = fXYZ_AnimationCurve.fY.Evaluate (fCurrent_Time / fTotal_Time);

                    transform.localPosition = new Vector3 (fFrom.x + (fTo.x - fFrom.x) * evaluate_x, fFrom.y + (fTo.y - fFrom.y) * evaluate_y, 0);

                    yield return null;
                }

                if (fCurrent_Time >= fTotal_Time)
                {
                    fStatus = TweenStatus.Stop;
                }

                callback (fStatus);

                if (fLoop == false)
                {
                    break;
                }
                else
                {
                    fCurrent_Time = 0;
                }
            }
        }

        private bool Judge ()
        {
            if (fColumn == Princess.Instance.fColumn && fRow == Princess.Instance.fRow)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}