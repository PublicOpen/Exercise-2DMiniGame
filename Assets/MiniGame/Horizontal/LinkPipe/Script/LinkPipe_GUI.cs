﻿using MapleStar.Timer;
using MapleStar.Tween;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.LinkPipe
{
    public class LinkPipe_GUI : MonoBehaviour
    {
        public static LinkPipe_GUI Instance;
        private GameStatus fStatus;
        public GameStatus pStatus
        {
            get
            {
                return fStatus;
            }
            set
            {
                fStatus = value;

                switch (value)
                {
                    case GameStatus.CountDown:
                        Time.timeScale = 1;

                        Reset_Game ();

                        fPanel_CountDown.gameObject.SetActive (true);
                        fPanel_CountDown.Play (0, (TweenStatus s) => {
                            fTime.Play ((TimerFinishStatus s2) => {
                                if (s2 == TimerFinishStatus.Normal)
                                {
                                    Worker.Instance.Move (Direction.Right);
                                }
                            });

                            pStatus = GameStatus.Playing;
                        });

                        break;
                    case GameStatus.Playing:
                        Time.timeScale = 1;

                        break;
                    case GameStatus.Pause:
                        Time.timeScale = 0;

                        fPanel_Pause.gameObject.SetActive (true);

                        break;
                    case GameStatus.Win:
                        End_Game ();
                        fPanel_Result.Show (pStatus);

                        break;
                    case GameStatus.Lose:
                        End_Game ();
                        fPanel_Result.Show (pStatus);

                        break;
                }
            }
        }

        public TimerCountDown fTime;

        [Header ("Visual UI")]
        public Button fBtn_Pause;

        [Header ("Panel")]
        public CountDownPanel fPanel_CountDown;
        public PausePanel fPanel_Pause;
        public ResultPanel fPanel_Result;

        void Awake ()
        {
            Instance = this;
        }

        IEnumerator Start ()
        {
            fTime.Setup (new TimeSpan (0, 0, 5));

            fBtn_Pause.onClick.AddListener (OnClick_Pause);

            yield return new WaitForEndOfFrame ();

            pStatus = GameStatus.CountDown;
        }

        void Update ()
        {
            /* 鍵盤事件 */
            if (Input.GetKeyDown (KeyCode.Escape))
            {
                if (pStatus == GameStatus.Playing)
                {
                    OnClick_Pause ();
                }
                else if (pStatus == GameStatus.Pause)
                {
                    fPanel_Pause.OnClick_Continue ();
                }
            }
        }

        private void Reset_Game ()
        {
            Worker.Instance.Stop ();
            LinkPipeSystem.Instance.SetUp_GameStart ();
        }

        public void End_Game ()
        {
            fTime.Stop ();
        }

        #region UI
        private void OnClick_Pause ()
        {
            pStatus = GameStatus.Pause;
        }
        #endregion
    }
}