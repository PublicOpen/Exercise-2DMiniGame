﻿using System;
using UnityEngine;

namespace MapleStar.LinkPipe
{
    [CreateAssetMenu (fileName = "LinkPipe Setting", menuName = "Custom ScriptableObject/LinkPipe/LinkPipe Setting")]
    public class LinkPipeSetting : ScriptableObject
    {
        public Vector2 fXY;

        [Space (10)]
        public CharacterPos[] fCharacterPos;
        public SpliceRegion[] fSpliceRegion;
        public MaterialRegion[] fMaterialRegion;
    }

    [Serializable]
    public struct CharacterPos
    {
        public Vector2 fWorkerPos;
        public Vector2 fPrincessPos;
    }

    #region Splice Region 拼接區
    [Serializable]
    public struct SpliceRegion
    {
        public Matrix[] fX_Column;
    }

    [Serializable]
    public struct Matrix
    {
        public bool[] fY_Row;
    }
    #endregion

    #region Material Region 原料區
    [Serializable]
    public struct MaterialRegion
    {
        public PipeCombination[] fPipeCombination;
    }

    [Serializable]
    public struct PipeCombination
    {
        public int[] fPipe;
    }
    #endregion
}