﻿using MapleStar.Tween;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.IconMatch
{
    public class Card : TweenRotation
    {
        public Button fBtn_Card;
        public Image fImg_Icon;

        void Start () 
        {
            fBtn_Card.onClick.AddListener (OnClick_Card);
        }

        public void Show ()
        {
            fTotal_Time = 3;
            Play (0, (TweenStatus s) => {
                IconMatchSystem.Instance.fMultiObjectWaiting.pCount++;
            });
        }

        private void TurnOver ()
        {
            fTotal_Time = 0.4f;
            Play (1, (TweenStatus s) => {
                IconMatchSystem.Instance.Match (this);
            });
        }

        public void Reverse ()
        {
            fTotal_Time = 0.4f;
            Play (2, (TweenStatus s) => {
                IconMatchSystem.Instance.fMultiObjectWaiting.pCount++;
            });
        }

        protected override IEnumerator _Play (Action<TweenStatus> callback)
        {
            while (true)
            {
                fStatus = TweenStatus.Play;

                while (fCurrent_Time < fTotal_Time && fStatus == TweenStatus.Play)
                {
                    fCurrent_Time += Time.deltaTime * fSpeed;

                    float evaluate_y = fXYZ_AnimationCurve.fY.Evaluate (fCurrent_Time / fTotal_Time);
                    float evaluate_z = fXYZ_AnimationCurve.fZ.Evaluate (fCurrent_Time / fTotal_Time);

                    transform.localEulerAngles = new Vector3 (0, evaluate_y * 360, evaluate_z * 5);

                    yield return null;
                }

                if (fCurrent_Time >= fTotal_Time)
                {
                    fStatus = TweenStatus.Stop;
                }

                callback (fStatus);

                if (fLoop == false)
                {
                    break;
                }
                else
                {
                    fCurrent_Time = 0;
                }
            }
        }

        #region UI
        private void OnClick_Card ()
        {
            if (IconMatchSystem.Instance.pStatus == IconCardStatus.CanTurnOver)
            {
                fBtn_Card.interactable = false;
                IconMatchSystem.Instance.fMultiObjectWaiting.pCount++;

                TurnOver ();
            }
        }
        #endregion
    }
}