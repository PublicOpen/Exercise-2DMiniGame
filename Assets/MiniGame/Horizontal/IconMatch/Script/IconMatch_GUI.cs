﻿using MapleStar.Timer;
using MapleStar.Tween;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.IconMatch
{
    public class IconMatch_GUI : MonoBehaviour
    {
        public static IconMatch_GUI Instance;
        private GameStatus fStatus;
        public GameStatus pStatus
        {
            get
            {
                return fStatus;
            }
            set
            {
                fStatus = value;

                switch (value)
                {
                    case GameStatus.CountDown:
                        Time.timeScale = 1;

                        Reset_Game ();

                        fPanel_CountDown.gameObject.SetActive (true);
                        fPanel_CountDown.Play (0, (TweenStatus s) => {
                            fScore.Begin_NumberJump ();
                            IconMatchSystem.Instance.pStatus = IconCardStatus.ShowAll;
                            pStatus = GameStatus.Playing;
                        });

                        break;
                    case GameStatus.Playing:
                        Time.timeScale = 1;

                        fTime.Play ((TimerFinishStatus s) => {
                            if (s == TimerFinishStatus.Normal)
                            {
                                pStatus = GameStatus.Win;
                            }
                        });

                        break;
                    case GameStatus.Pause:
                        Time.timeScale = 0;

                        fTime.Pause ();
                        fPanel_Pause.gameObject.SetActive (true);

                        break;
                    case GameStatus.Win:
                        End_Game ();
                        fPanel_Result.Show (pStatus);

                        break;
                    case GameStatus.Lose:
                        End_Game ();
                        fPanel_Result.Show (pStatus);

                        break;
                }
            }
        }

        [Header ("Visual UI")]
        public Slider fSlider_Time_Bar;
        public TimerCountDown fTime;

        [Space (10)]
        public Text fTxt_Score;
        public NumberJump fScore;

        [Space (10)]
        public Text fTxt_Combo;
        public TweenScale fCombo;
        public int pCombo
        {
            get
            {
                return Convert.ToInt32 (fTxt_Combo.text);
            }
            set
            {
                fTxt_Combo.text = value.ToString ();
                fCombo.Play (0, (TweenStatus s) => {});
            }
        }
        public Button fBtn_Pause;

        [Header ("Panel")]
        public CountDownPanel fPanel_CountDown;
        public PausePanel fPanel_Pause;
        public ResultPanel fPanel_Result;

        void Awake ()
        {
            Instance = this;
        }

        void OnEnable ()
        {
            IconMatchSystem.eMatchResult += MatchResult;
        }

        void Start () 
        {
            fTime.GetTime ((double d) => {
                fSlider_Time_Bar.value = Convert.ToSingle (d);
            });
            fTime.Setup (new TimeSpan (0, 0, 180));

            fScore.GetNumber ((float f) => {
                fTxt_Score.text = string.Format ("{0:D7}", Convert.ToInt32 (f));
            });

            fBtn_Pause.onClick.AddListener (OnClick_Pause);

            pStatus = GameStatus.CountDown;
        }

        void Update ()
        {
            /* 鍵盤事件 */
            if (Input.GetKeyDown (KeyCode.Escape))
            {
                if (pStatus == GameStatus.Playing)
                {
                    OnClick_Pause ();
                }
                else if (pStatus == GameStatus.Pause)
                {
                    fPanel_Pause.OnClick_Continue ();
                }
            }
        }

        void OnDisable ()
        {
            IconMatchSystem.eMatchResult -= MatchResult;
        }

        private void Reset_Game ()
        {
            fSlider_Time_Bar.value = fSlider_Time_Bar.maxValue;
            fScore.ResetValue ();
            fTxt_Combo.text = "0";

            IconMatchSystem.Instance.pStatus = IconCardStatus.Setup;
        }

        public void End_Game ()
        {
            fTime.Stop ();

            fScore.Stop_NumberJump ();
        }

        private void MatchResult (bool b)
        {
            if (b == true)
            {
                fScore.pTarget_Number += 100;
                pCombo++;
            }
            else
            {
                pCombo = 0;
            }
        }

        #region UI
        private void OnClick_Pause ()
        {
            pStatus = GameStatus.Pause;
        }
        #endregion
    }
}