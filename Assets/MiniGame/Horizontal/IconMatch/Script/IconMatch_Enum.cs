﻿namespace MapleStar.IconMatch
{
    public enum GameStatus
    {
        CountDown,
        Playing,
        Pause,
        Win,
        Lose
    }

    public enum IconCardStatus
    {
        Setup, // 分配牌的位置
        ShowAll, // 全牌展示
        CanTurnOver, // 可翻牌
        WaitingTurnOver, // 等待兩張都翻面過來
        MatchFailure, // 配對失敗
        MatchSuccess // 配對成功
    }
}