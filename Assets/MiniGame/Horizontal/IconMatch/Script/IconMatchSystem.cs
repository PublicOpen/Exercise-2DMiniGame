﻿using MapleStar.Timer;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.IconMatch
{
    public class IconMatchSystem : MonoBehaviour
    {
        public static IconMatchSystem Instance;
        private IconCardStatus fStatus;
        public IconCardStatus pStatus
        {
            get
            {
                return fStatus;
            }
            set
            {
                fStatus = value;

                switch (value)
                {
                    case IconCardStatus.Setup:
                        Setup_CardIcon ();

                        break;
                    case IconCardStatus.ShowAll:
                        fMultiObjectWaiting.Wait (fCards.Length, () => {
                            pStatus = IconCardStatus.CanTurnOver;
                        });

                        foreach (Card card in fCards)
                        {
                            card.Show ();
                        }

                        break;
                    case IconCardStatus.CanTurnOver:
                        fMultiObjectWaiting.Wait (2, () => {
                            pStatus = IconCardStatus.WaitingTurnOver;
                        });

                        break;
                    case IconCardStatus.WaitingTurnOver:


                        break;
                    case IconCardStatus.MatchFailure:
                        if (eMatchResult != null)
                        {
                            eMatchResult (false);
                        }

                        fMultiObjectWaiting.Wait (2, () => {
                            fSelected_Cards.Clear ();
                            pStatus = IconCardStatus.CanTurnOver;
                        });

                        foreach (Card card in fSelected_Cards)
                        {
                            card.fBtn_Card.interactable = true;
                            card.Reverse ();
                        }

                        break;
                    case IconCardStatus.MatchSuccess:
                        if (eMatchResult != null)
                        {
                            eMatchResult (true);
                        }

                        foreach (Card card in fSelected_Cards)
                        {
                            card.fBtn_Card.interactable = true;
                            card.gameObject.SetActive (false);
                            card.transform.localEulerAngles = Vector3.zero;
                            fRemaining_Cards.Remove (card);
                        }

                        fSelected_Cards.Clear ();

                        if (fRemaining_Cards.Count == 0)
                        {
                            pStatus = IconCardStatus.Setup;
                            pStatus = IconCardStatus.ShowAll;
                        }
                        else
                        {
                            pStatus = IconCardStatus.CanTurnOver;
                        }

                        break;
                }
            }
        }

        public MultiObjectWaiting fMultiObjectWaiting;

        [Header ("Card Region")]
        public RectTransform fCard_Region;
        private const int fNumber_Of_Card = 18;
        public GameObject fPrefab_Card;
        public Sprite[] fIconSprite;

        [Header ("Record Use")]
        private Card[] fCards = new Card[fNumber_Of_Card];
        public List<Card> fRemaining_Cards = new List<Card> ();
        public List<Card> fSelected_Cards = new List<Card> ();

        public static event Action<bool> eMatchResult;

        void Awake ()
        {
            Instance = this;
        }

        IEnumerator Start () 
        {
            for (int i = 0; i < fNumber_Of_Card; i++)
            {
                GameObject obj = Instantiate (fPrefab_Card, fCard_Region);
                fCards[i] = obj.GetComponent<Card> ();
            }

            yield return new WaitForEndOfFrame ();

            fCard_Region.GetComponent<GridLayoutGroup> ().enabled = false;
        }

        private void Setup_CardIcon ()
        {
            fRemaining_Cards.Clear (); // 如果是暫停後重新開始，就需要 Clear fRemaining_Cards

            foreach (Card card in fCards)
            {
                card.Stop ();
                card.transform.localEulerAngles = new Vector3 (0, 0, -5);
            }

            /* 從 fIconSprite 隨機選 "牌的總數／2" 種出來 */
            int[] index_1 = ArrayListEx.DisruptSort (fIconSprite.Length);
            List<Sprite> temp_icon_sprite = new List<Sprite> ();

            for (int i = 0; i < fNumber_Of_Card / 2; i++)
            {
                temp_icon_sprite.Add (fIconSprite[index_1[i]]);
            }

            /* fIconSprite 隨機排列給 Card */
            int[] index_2 = ArrayListEx.DisruptSort (fNumber_Of_Card);

            for (int i = 0; i < fNumber_Of_Card; i++)
            {
                fCards[index_2[i]].gameObject.SetActive (true);
                fCards[index_2[i]].fImg_Icon.sprite = temp_icon_sprite[i % fNumber_Of_Card / 2];
                fRemaining_Cards.Add (fCards[index_2[i]]);
            }
        }

        public void Match (Card card)
        {
            fSelected_Cards.Add (card);

            if (fSelected_Cards.Count == 2)
            {
                StartCoroutine (_Match ());
            }
        }

        private IEnumerator _Match ()
        {
            yield return new WaitForSeconds (0.25f);

            if (fSelected_Cards[0].fImg_Icon.sprite == fSelected_Cards[1].fImg_Icon.sprite)
            {
                pStatus = IconCardStatus.MatchSuccess;
            }
            else
            {
                pStatus = IconCardStatus.MatchFailure;
            }
        }
    }
}