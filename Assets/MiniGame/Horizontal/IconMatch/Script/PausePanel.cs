﻿using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.IconMatch
{
    public class PausePanel : MonoBehaviour
    {
        public Button fBtn_Continue;
        public Button fBtn_Restart;
        public Button fBtn_Exit;

        void Start ()
        {
            fBtn_Continue.onClick.AddListener (OnClick_Continue);
            fBtn_Restart.onClick.AddListener (OnClick_Restart);
            fBtn_Exit.onClick.AddListener (OnClick_Exit);
        }

        #region UI
        public void OnClick_Continue ()
        {
            gameObject.SetActive (false);
            IconMatch_GUI.Instance.pStatus = GameStatus.Playing;
        }

        private void OnClick_Restart ()
        {
            gameObject.SetActive (false);
            IconMatch_GUI.Instance.End_Game ();
            IconMatch_GUI.Instance.pStatus = GameStatus.CountDown;
        }

        private void OnClick_Exit ()
        {
            Application.Quit ();
        }
        #endregion
    }
}