﻿using MapleStar.Tween;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.IconMatch
{
    public class Combo : TweenScale
    {
        public Text fTxt_Combo;
        public Text fTxt_Text;

        protected override IEnumerator _Play (Action<TweenStatus> callback)
        {
            while (true)
            {
                fStatus = TweenStatus.Play;

                while (fCurrent_Time < fTotal_Time && fStatus == TweenStatus.Play)
                {
                    fCurrent_Time += Time.deltaTime * fSpeed;

                    float evaluate_x = fXYZ_AnimationCurve.fX.Evaluate (fCurrent_Time / fTotal_Time);
                    float evaluate_y = fXYZ_AnimationCurve.fY.Evaluate (fCurrent_Time / fTotal_Time);

                    fTxt_Combo.transform.localScale = new Vector3 (evaluate_x, evaluate_y, 0);
                    fTxt_Text.transform.localScale = new Vector3 (evaluate_x, evaluate_y, 0);

                    yield return null;
                }

                if (fCurrent_Time >= fTotal_Time)
                {
                    fStatus = TweenStatus.Stop;
                }

                callback (fStatus);

                if (fLoop == false)
                {
                    break;
                }
                else
                {
                    fCurrent_Time = 0;
                }
            }
        }
    }
}