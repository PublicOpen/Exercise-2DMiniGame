﻿namespace MapleStar.Gomoku
{
    public enum GameStatus
    {
        Prepare,
        Player1,
        Player2,
        Load,
        Win,
        Tie,
        Lose
    }

    public enum CheckerboardCoordinate : int
    {
        A = 1,
        B = 2,
        C = 3,
        D = 4,
        E = 5,
        F = 6,
        G = 7,
        H = 8,
        I = 9
    }

    public enum ChessType
    {
        None,
        Black,
        White
    }
}