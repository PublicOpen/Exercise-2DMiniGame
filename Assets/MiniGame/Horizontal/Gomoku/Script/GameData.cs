﻿using System;
using System.Collections.Generic;

namespace MapleStar.Gomoku
{
    [Serializable]
    public struct GameData
    {
        /* 黑先白後 Black / White */
        public string fPlayer1_Symbol;
        public string fPlayer2_Symbol;
        public List<Chess> fCheckerboard_Data; // 棋盤上的每顆棋子位置、歸屬
    }

    [Serializable]
    public struct Chess
    {
        public int fOrder;
        public string fType;
        /// <summary>
        /// X
        /// </summary>
        public string fColumn;
        /// <summary>
        /// Y
        /// </summary>
        public int fRow;

        public Chess (int order, ChessType type, int column, int row)
        {
            fOrder = order;
            fType = type.ToString ();
            fColumn = ((CheckerboardCoordinate)column).ToString ();
            fRow = row;
        }
    }
}