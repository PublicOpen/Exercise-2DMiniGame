﻿using Facebook.MiniJSON;
using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.Gomoku
{
    public class Gomoku_GUI : MonoBehaviour
    {
        public static Gomoku_GUI Instance;
        private GameStatus fStatus;
        public GameStatus pStatus
        {
            get
            {
                return fStatus;
            }
            set
            {
                fStatus = value;

                switch (value)
                {
                    case GameStatus.Prepare:
                        fTxt_Message.text = "";
                        fBtn_PlayerFirst.interactable = true;
                        fBtn_ComputerFirst.interactable = true;
                        fBtn_Restart.interactable = false;
                        fBtn_Save.interactable = false;

                        GomokuSystem.Instance.Setup_GameStart ();

                        break;
                    case GameStatus.Player1:
                        fBtn_Save.interactable = true;
                        fTxt_Message.text = "輪到你";

                        break;
                    case GameStatus.Player2:
                        fBtn_Save.interactable = false;
                        fTxt_Message.text = "輪到電腦";
                        GomokuSystem.Instance.AI ();

                        break;
                    case GameStatus.Load:
                        fBtn_PlayerFirst.interactable = false;
                        fBtn_ComputerFirst.interactable = false;
                        fBtn_Restart.interactable = true;
                        fBtn_Save.interactable = true;

                        /* 1. 先清空棋盤、_Point，回到開局狀態 */
                        GomokuSystem.Instance.Setup_GameStart ();

                        /* 2. 資料取出 */
                        JsonFile.Instance.Load (Application.persistentDataPath + "/Save.dat", (string json) => {
                            IDictionary chess_manual = (IDictionary)Json.Deserialize (json);
                            GomokuSystem.Instance.Setup_PlayerSymbol (EnumEx.ToEnum<ChessType> (chess_manual["fPlayer1_Symbol"].ToString ()), EnumEx.ToEnum<ChessType> (chess_manual["fPlayer2_Symbol"].ToString ()));
                            IList checkerboard_data = (IList)chess_manual["fCheckerboard_Data"];

                            foreach (IDictionary record in checkerboard_data)
                            {
                                GomokuSystem.Instance.fPoints[Convert.ToInt32 (EnumEx.ToEnum<CheckerboardCoordinate> (record["fColumn"].ToString ())) - 1, Convert.ToInt32 (record["fRow"]) - 1].pType = EnumEx.ToEnum<ChessType> (record["fType"].ToString ());
                            }

                            /* 3. 因為能「存、讀檔」時，一定是玩家的回合，因此... */
                            pStatus = GameStatus.Player1;
                        });

                        break;
                    case GameStatus.Win:
                        fBtn_Save.interactable = false;
                        pWin++;
                        fTxt_Message.text = "<color=#ff0000ff>你贏了</color>";

                        break;
                    case GameStatus.Tie:
                        fBtn_Save.interactable = false;
                        pTie++;
                        fTxt_Message.text = "<color=#008000ff>平手</color>";

                        break;
                    case GameStatus.Lose:
                        fBtn_Save.interactable = false;
                        pLose++;
                        fTxt_Message.text = "<color=#000080ff>你GG了</color>";

                        break;
                }
            }
        }

        [Header ("View")]
        public Text fTxt_Message;

        [Header ("Scoreboard")]
        public Text fTxt_Win;
        private int pWin
        {
            get
            {
                return Convert.ToInt32 (fTxt_Win.text);
            }
            set
            {
                PlayerPrefs.SetInt ("Win", value);
                fTxt_Win.text = value.ToString ();
            }
        }
        public Text fTxt_Tie;
        private int pTie
        {
            get
            {
                return Convert.ToInt32 (fTxt_Tie.text);
            }
            set
            {
                PlayerPrefs.SetInt ("Tie", value);
                fTxt_Tie.text = value.ToString ();
            }
        }
        public Text fTxt_Lose;
        private int pLose
        {
            get
            {
                return Convert.ToInt32 (fTxt_Lose.text);
            }
            set
            {
                PlayerPrefs.SetInt ("Lose", value);
                fTxt_Lose.text = value.ToString ();
            }
        }

        [Header ("Right-Menu")]
        public Button fBtn_PlayerFirst;
        public Button fBtn_ComputerFirst;
        public Button fBtn_Restart;
        public Button fBtn_Save;
        public Button fBtn_Load;
        public Button fBtn_Afresh;

        [Space (10)]
        public Button fBtn_Exit;

        void Awake ()
        {
            Instance = this;
        }

        void Start ()
        {
            pWin = PlayerPrefs.GetInt ("Win");
            pTie = PlayerPrefs.GetInt ("Tie");
            pLose = PlayerPrefs.GetInt ("Lose");

            fBtn_PlayerFirst.onClick.AddListener (OnClick_PlayerFirst);
            fBtn_ComputerFirst.onClick.AddListener (OnClick_ComputerFirst);
            fBtn_Restart.onClick.AddListener (OnClick_Restart);
            fBtn_Save.onClick.AddListener (OnClick_Save);
            fBtn_Load.onClick.AddListener (OnClick_Load);
            fBtn_Afresh.onClick.AddListener (OnClick_Afresh);
            fBtn_Exit.onClick.AddListener (OnClick_Exit);

            if (!File.Exists (Application.persistentDataPath + "/Save.dat"))
            {
                fBtn_Load.interactable = false;
            }

            pStatus = GameStatus.Prepare;
        }

        #region UI
        private void OnClick_PlayerFirst ()
        {
            fBtn_PlayerFirst.interactable = false;
            fBtn_ComputerFirst.interactable = false;
            fBtn_Restart.interactable = true;
            fBtn_Save.interactable = true;

            GomokuSystem.Instance.Setup_PlayerSymbol (ChessType.Black, ChessType.White);

            pStatus = GameStatus.Player1;
        }

        private void OnClick_ComputerFirst ()
        {
            fBtn_PlayerFirst.interactable = false;
            fBtn_ComputerFirst.interactable = false;
            fBtn_Restart.interactable = true;
            fBtn_Save.interactable = true;

            GomokuSystem.Instance.Setup_PlayerSymbol (ChessType.White, ChessType.Black);

            pStatus = GameStatus.Player2;
        }

        private void OnClick_Restart ()
        {
            pStatus = GameStatus.Prepare;
        }

        private void OnClick_Save ()
        {
            JsonFile.Instance.Write (Application.persistentDataPath + "/Save.dat", JsonUtility.ToJson (GomokuSystem.Instance.fChess_Manual, true), () => {
                fBtn_Load.interactable = true;
            });
        }

        private void OnClick_Load ()
        {
            pStatus = GameStatus.Load;
        }

        private void OnClick_Afresh ()
        {
            pWin = 0;
            pTie = 0;
            pLose = 0;
        }

        private void OnClick_Exit ()
        {
            Application.Quit ();
        }
        #endregion
    }
}