﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapleStar.Gomoku
{
    public class GomokuSystem : MonoBehaviour
    {
        public static GomokuSystem Instance;

        [Header ("Checkerboard")]
        public Transform fCheckerboard;
        /// <summary>
        /// X
        /// </summary>
        private const int fColumn = 9;
        /// <summary>
        /// Y
        /// </summary>
        private const int fRow = 9;
        public GameObject fPrefab_Point;
        public GameObject fPrefab_BlackChess;
        public GameObject fPrefab_WhiteChess;
        public GameData fChess_Manual; // 棋譜
        public Point[,] fPoints = new Point[fColumn, fRow]; // 棋盤格全按鈕，ReStart時，還原會用到
        public List<Point> fUnused_Points = new List<Point> (); // 計算用，每下一手-1

        [Header ("Player's Symbol")]
        public ChessType fPlayer1_Symbol;
        public ChessType fPlayer2_Symbol;

        void Awake ()
        {
            Instance = this;
        }

        void Start ()
        {
            for (int i = 1; i <= fColumn; i++)
            {
                for (int j = 1; j <= fRow; j++)
                {
                    GameObject obj = Instantiate (fPrefab_Point, fCheckerboard);

                    #if UNITY_EDITOR
                    obj.name = string.Format ("{0},{1}", (CheckerboardCoordinate)i, j);
                    #endif

                    Point point = obj.GetComponent<Point> ();
                    point.fColumn = i;
                    point.fRow = j;
                    fPoints[i - 1, j - 1] = point;
                }
            }
        }

        public void Setup_GameStart ()
        {
            fChess_Manual = new GameData ();
            fChess_Manual.fCheckerboard_Data = new List<Chess> ();
            fUnused_Points.Clear ();

            foreach (Point point in fPoints)
            {
                point.pType = ChessType.None;
                fUnused_Points.Add (point);
            }
        }

        public void Setup_PlayerSymbol (ChessType player1_symbol, ChessType player2_symbol)
        {
            fPlayer1_Symbol = player1_symbol;
            fPlayer2_Symbol = player2_symbol;

            fChess_Manual.fPlayer1_Symbol = fPlayer1_Symbol.ToString ();
            fChess_Manual.fPlayer2_Symbol = fPlayer2_Symbol.ToString ();
        }

        public GameStatus Judge (ChessType symbol, Point point)
        {
            int column = point.fColumn;
            int row = point.fRow;

            if (A (symbol, column, row) + E (symbol, column, row) >= 4 || B (symbol, column, row) + F (symbol, column, row) >= 4 || C (symbol, column, row) + G (symbol, column, row) >= 4 || D (symbol, column, row) + H (symbol, column, row) >= 4)
            {
                if (symbol == fPlayer1_Symbol)
                {
                    return GameStatus.Win;
                }
                else if (symbol == fPlayer2_Symbol)
                {
                    return GameStatus.Lose;
                }
            }
            if (fUnused_Points.Count == 0) // 全棋盤都下完，依然不分勝負
            {
                return GameStatus.Tie;
            }
            else if (symbol == fPlayer1_Symbol)
            {
                return GameStatus.Player2;
            }
            else //if (symbol == fPlayer2_Symbol)
            {
                return GameStatus.Player1;
            }
        }

        #region 8方位計算-連鎖棋子數
        private int A (ChessType symbol, int column, int row) // ↑
        {
            int sum = 0;

            for (int i = 1; i < 5; i++)
            {
                if (CheckPointExist (column, row + i) == true)
                {
                    if (fPoints[column - 1, row + i - 1].pType == symbol)
                    {
                        sum++;
                    }
                    else
                    {
                        return sum;
                    }
                }
                else
                {
                    return sum;
                }
            }

            return sum;
        }

        private int B (ChessType symbol, int column, int row) // ↗
        {
            int sum = 0;

            for (int i = 1; i < 5; i++)
            {
                if (CheckPointExist (column + i, row + i) == true)
                {
                    if (fPoints[column + i - 1, row + i - 1].pType == symbol)
                    {
                        sum++;
                    }
                    else
                    {
                        return sum;
                    }
                }
                else
                {
                    return sum;
                }
            }

            return sum;
        }

        private int C (ChessType symbol, int column, int row) // →
        {
            int sum = 0;

            for (int i = 1; i < 5; i++)
            {
                if (CheckPointExist (column + i, row) == true)
                {
                    if (fPoints[column + i - 1, row - 1].pType == symbol)
                    {
                        sum++;
                    }
                    else
                    {
                        return sum;
                    }
                }
                else
                {
                    return sum;
                }
            }

            return sum;
        }

        private int D (ChessType symbol, int column, int row) // ↘
        {
            int sum = 0;

            for (int i = 1; i < 5; i++)
            {
                if (CheckPointExist (column + i, row - i) == true)
                {
                    if (fPoints[column + i - 1, row - i - 1].pType == symbol)
                    {
                        sum++;
                    }
                    else
                    {
                        return sum;
                    }
                }
                else
                {
                    return sum;
                }
            }

            return sum;
        }

        private int E (ChessType symbol, int column, int row) // ↓
        {
            int sum = 0;

            for (int i = 1; i < 5; i++)
            {
                if (CheckPointExist (column, row - i) == true)
                {
                    if (fPoints[column - 1, row - i - 1].pType == symbol)
                    {
                        sum++;
                    }
                    else
                    {
                        return sum;
                    }
                }
                else
                {
                    return sum;
                }
            }

            return sum;
        }

        private int F (ChessType symbol, int column, int row) // ↙
        {
            int sum = 0;

            for (int i = 1; i < 5; i++)
            {
                if (CheckPointExist (column - i, row - i) == true)
                {
                    if (fPoints[column - i - 1, row - i - 1].pType == symbol)
                    {
                        sum++;
                    }
                    else
                    {
                        return sum;
                    }
                }
                else
                {
                    return sum;
                }
            }

            return sum;
        }

        private int G (ChessType symbol, int column, int row) // ←
        {
            int sum = 0;

            for (int i = 1; i < 5; i++)
            {
                if (CheckPointExist (column - i, row) == true)
                {
                    if (fPoints[column - i - 1, row - 1].pType == symbol)
                    {
                        sum++;
                    }
                    else
                    {
                        return sum;
                    }
                }
                else
                {
                    return sum;
                }
            }

            return sum;
        }

        private int H (ChessType symbol, int column, int row) // ↖
        {
            int sum = 0;

            for (int i = 1; i < 5; i++)
            {
                if (CheckPointExist (column - i, row + i) == true)
                {
                    if (fPoints[column - i - 1, row + i - 1].pType == symbol)
                    {
                        sum++;
                    }
                    else
                    {
                        return sum;
                    }
                }
                else
                {
                    return sum;
                }
            }

            return sum;
        }
        #endregion

        #region AI
        public void AI ()
        {
            StartCoroutine (_AI ());
        }

        private IEnumerator _AI ()
        {
            yield return new WaitForSeconds (0.5f);

            if (fChess_Manual.fCheckerboard_Data.Count > 0)
            {
                List<Point> possible_selection_points = CheckPoints (fPoints[Convert.ToInt32 (EnumEx.ToEnum<CheckerboardCoordinate> (fChess_Manual.fCheckerboard_Data[fChess_Manual.fCheckerboard_Data.Count - 1].fColumn)) - 1, fChess_Manual.fCheckerboard_Data[fChess_Manual.fCheckerboard_Data.Count - 1].fRow - 1]); // AI可能會下的位置

                if (possible_selection_points.Count != 0) // 會玩家所下的棋子位置，周圍選一點下
                {
                    possible_selection_points[UnityEngine.Random.Range (0, possible_selection_points.Count)].pType = fPlayer2_Symbol;
                }
                else // 玩家所下的棋子位置，周圍已無空位，就隨機下一子
                {
                    fUnused_Points[UnityEngine.Random.Range (0, fUnused_Points.Count)].pType = fPlayer2_Symbol;
                }
            }
            else
            {
                fPoints[(fColumn - 1) / 2 , (fRow - 1) / 2].pType = fPlayer2_Symbol; // 天元
            }
        }

        private List<Point> CheckPoints (Point reference_point) // 檢查「點」是否存在
        {
            List<Point> temp_points = new List<Point> (); // AI可能會下的位置

            /* 檢查8方位的點是否存在 */
            if (CheckPoint (reference_point.fColumn, reference_point.fRow + 1) == true) // ↑
            {
                temp_points.Add (fPoints[reference_point.fColumn - 1, reference_point.fRow]);
            }
            if (CheckPoint (reference_point.fColumn + 1, reference_point.fRow + 1) == true) // ↗
            {
                temp_points.Add (fPoints[reference_point.fColumn, reference_point.fRow]);
            }
            if (CheckPoint (reference_point.fColumn + 1, reference_point.fRow) == true) // →
            {
                temp_points.Add (fPoints[reference_point.fColumn, reference_point.fRow - 1]);
            }
            if (CheckPoint (reference_point.fColumn + 1, reference_point.fRow - 1) == true) // ↘
            {
                temp_points.Add (fPoints[reference_point.fColumn, reference_point.fRow - 2]);
            }
            if (CheckPoint (reference_point.fColumn, reference_point.fRow - 1) == true) // ↓
            {
                temp_points.Add (fPoints[reference_point.fColumn - 1, reference_point.fRow - 2]);
            }
            if (CheckPoint (reference_point.fColumn - 1, reference_point.fRow - 1) == true) // ↙
            {
                temp_points.Add (fPoints[reference_point.fColumn - 2, reference_point.fRow - 2]);
            }
            if (CheckPoint (reference_point.fColumn - 1, reference_point.fRow) == true) // ←
            {
                temp_points.Add (fPoints[reference_point.fColumn - 2, reference_point.fRow - 1]);
            }
            if (CheckPoint (reference_point.fColumn - 1, reference_point.fRow + 1) == true)// ↖
            {
                temp_points.Add (fPoints[reference_point.fColumn - 2, reference_point.fRow]);
            }

            return temp_points;
        }

        private bool CheckPoint (int column, int row)
        {
            if (CheckPointExist (column, row) == true)
            {
                if (CheckPointUse (column, row) == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        #endregion

        private bool CheckPointExist (int column, int row)
        {
            if (column > fColumn || row > fRow || column < 1 || row < 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool CheckPointUse (int column, int row)
        {
            if (fPoints[column - 1, row - 1].pType == ChessType.None)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}