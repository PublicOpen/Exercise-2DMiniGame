﻿using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.Gomoku
{
    public class Point : MonoBehaviour
    {
        public Button fBtn_Point;
        private ChessType fType;
        public ChessType pType
        {
            get
            {
                return fType;
            }
            set
            {
                fType = value;

                switch (fType)
                {
                    case ChessType.None:
                        TransformEx.ChildClear (transform);
                        fBtn_Point.interactable = true;

                        break;
                    case ChessType.Black:
                        Instantiate (GomokuSystem.Instance.fPrefab_BlackChess, transform);
                        After ();

                        break;
                    case ChessType.White:
                        Instantiate (GomokuSystem.Instance.fPrefab_WhiteChess, transform);
                        After ();

                        break;
                }
            }
        }
        /// <summary>
        /// X
        /// </summary>
        public int fColumn;
        /// <summary>
        /// Y
        /// </summary>
        public int fRow;

        void Start ()
        {
            fBtn_Point.onClick.AddListener (OnClick_Point);
        }

        private void After ()
        {
            GomokuSystem.Instance.fUnused_Points.Remove (this);
            GomokuSystem.Instance.fChess_Manual.fCheckerboard_Data.Add (new Chess (GomokuSystem.Instance.fChess_Manual.fCheckerboard_Data.Count + 1, fType, fColumn, fRow));
            fBtn_Point.interactable = false;

            if (Gomoku_GUI.Instance.pStatus == GameStatus.Player1 || Gomoku_GUI.Instance.pStatus == GameStatus.Player2)
            {
                Gomoku_GUI.Instance.pStatus = GomokuSystem.Instance.Judge (fType, this);
            }
        }

        #region UI
        private void OnClick_Point ()
        {
            if (Gomoku_GUI.Instance.pStatus == GameStatus.Player1)
            {
                pType = GomokuSystem.Instance.fPlayer1_Symbol;
            }
        }
        #endregion
    }
}