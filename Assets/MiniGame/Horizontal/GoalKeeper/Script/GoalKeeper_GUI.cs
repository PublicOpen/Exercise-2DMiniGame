﻿using MapleStar.Timer;
using MapleStar.Tween;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.GoalKeeper
{
    public class GoalKeeper_GUI : MonoBehaviour
    {
        public static GoalKeeper_GUI Instance;
        private GameStatus fStatus;
        public GameStatus pStatus
        {
            get
            {
                return fStatus;
            }
            set
            {
                fStatus = value;

                switch (value)
                {
                    case GameStatus.CountDown:
                        Time.timeScale = 1;

                        Reset_Game ();

                        fPanel_CountDown.gameObject.SetActive (true);
                        fPanel_CountDown.Play (0, (TweenStatus s) => {
                            SoccerPenaltySystem.Instance.Begin_PenaltyKick ();
                            pStatus = GameStatus.Playing;
                        });

                        break;
                    case GameStatus.Playing:
                        Time.timeScale = 1;

                        fTime.Play ((TimerFinishStatus s) => {
                            if (s == TimerFinishStatus.Normal)
                            {
                                pStatus = GameStatus.Win;
                            }
                        });

                        break;
                    case GameStatus.Pause:
                        Time.timeScale = 0;

                        fTime.Pause ();
                        fPanel_Pause.gameObject.SetActive (true);

                        break;
                    case GameStatus.Win:
                        End_Game ();
                        fPanel_Result.Show (pStatus);

                        break;
                    case GameStatus.Lose:
                        End_Game ();
                        fPanel_Result.Show (pStatus);

                        break;
                }
            }
        }

        [Header ("Visual UI")]
        public Slider fSlider_Time_Bar;
        public TimerCountDown fTime;
        public Button fBtn_Pause;
        public Slider fSlider_Hp_Bar;
        public Text fTxt_Hp;
        private int fHp;
        public int pHp
        {
            get
            {
                return fHp;
            }
            set
            {
                fHp = value;
                fSlider_Hp_Bar.value = fHp;

                if (fHp > 0)
                {
                    fTxt_Hp.text = fHp.ToString ();
                }
                else
                {
                    fTxt_Hp.text = "0";
                    pStatus = GameStatus.Lose;
                }
            }
        }

        [Header ("Panel")]
        public CountDownPanel fPanel_CountDown;
        public PausePanel fPanel_Pause;
        public ResultPanel fPanel_Result;

        void Awake ()
        {
            Instance = this;
        }

        void Start ()
        {
            fTime.GetTime ((double d) => {
                fSlider_Time_Bar.value = Convert.ToSingle (d);
            });
            fTime.Setup (new TimeSpan (0, 0, 60));

            fBtn_Pause.onClick.AddListener (OnClick_Pause);

            pStatus = GameStatus.CountDown;
        }

        void Update ()
        {
            /* 鍵盤事件 */
            if (Input.GetKeyDown (KeyCode.Escape))
            {
                if (pStatus == GameStatus.Playing)
                {
                    OnClick_Pause ();
                }
                else if (pStatus == GameStatus.Pause)
                {
                    fPanel_Pause.OnClick_Continue ();
                }
            }
        }

        private void Reset_Game ()
        {
            fSlider_Time_Bar.value = fSlider_Time_Bar.maxValue;
            pHp = Convert.ToInt32 (fSlider_Hp_Bar.maxValue);
        }

        public void End_Game ()
        {
            fTime.Stop ();

            SoccerPenaltySystem.Instance.Stop_PenaltyKick ();
            SoccerPenaltySystem.Instance.Clearance ();
        }

        #region UI
        private void OnClick_Pause ()
        {
            pStatus = GameStatus.Pause;
        }
        #endregion
    }
}