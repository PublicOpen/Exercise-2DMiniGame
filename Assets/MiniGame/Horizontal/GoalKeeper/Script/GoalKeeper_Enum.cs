﻿namespace MapleStar.GoalKeeper
{
    public enum GameStatus
    {
        CountDown,
        Playing,
        Pause,
        Win,
        Lose
    }
}