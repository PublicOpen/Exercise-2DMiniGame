﻿using System.Collections;
using UnityEngine;

namespace MapleStar.GoalKeeper
{
    public class SoccerPenaltySystem : MonoBehaviour
    {
        public static SoccerPenaltySystem Instance;

        [Header ("Penalty Area")]
        public Transform fPenaltyArea;
        public Transform fPenaltyPlayer;
        public GameObject fPrefab_Football;

        private float fFrequency = 1f; // 踢球的頻率
        private Coroutine fCoroutine_PenaltyKick;

        void Awake ()
        {
            Instance = this;
        }

        public void Begin_PenaltyKick ()
        {
            fCoroutine_PenaltyKick = StartCoroutine (_PenaltyKick ());
        }

        private IEnumerator _PenaltyKick ()
        {
            while (true)
            {
                Instantiate (fPrefab_Football, fPenaltyArea);

                yield return new WaitForSeconds (fFrequency);
            }
        }

        public void Stop_PenaltyKick ()
        {
            StopCoroutine (fCoroutine_PenaltyKick);
        }

        public void Clearance ()
        {
            Football[] footballs = fPenaltyArea.GetComponentsInChildren<Football> ();

            foreach (Football ball in footballs)
            {
                Destroy (ball.gameObject);
            }
        }
    }
}