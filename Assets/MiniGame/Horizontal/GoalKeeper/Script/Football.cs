﻿using MapleStar.Tween;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MapleStar.GoalKeeper
{
    public class Football : TweenPosition, IPointerDownHandler
    {
        private Vector3 fFrom;
        private Vector3 fTo;

        public GameObject fPrompt;

        void Start () 
        {
            fFrom = SoccerPenaltySystem.Instance.fPenaltyPlayer.position;
            fTo = new Vector3 (UnityEngine.Random.Range (-10f, 10f), UnityEngine.Random.Range (-10f, 10f), 30);

            Play (UnityEngine.Random.Range (0, fXYZ_AnimationCurves.Length), (TweenStatus s) => {
                GoalKeeper_GUI.Instance.pHp -= 10;
                Destroy (gameObject);
            });
        }

        void Update ()
        {
            if (GoalKeeper_GUI.Instance.pStatus != GameStatus.Playing && GoalKeeper_GUI.Instance.pStatus != GameStatus.Pause)
            {
                Destroy (gameObject);
            }

            if (transform.position.z < 50.0f)
            {
                fPrompt.SetActive (true);
            }
        }

        protected override IEnumerator _Play (Action<TweenStatus> callback)
        {
            while (true)
            {
                fStatus = TweenStatus.Play;

                while (fCurrent_Time < fTotal_Time && fStatus == TweenStatus.Play)
                {
                    fCurrent_Time += Time.deltaTime * fSpeed;

                    float evaluate_x = fXYZ_AnimationCurve.fX.Evaluate (fCurrent_Time / fTotal_Time);
                    float evaluate_y = fXYZ_AnimationCurve.fY.Evaluate (fCurrent_Time / fTotal_Time);
                    float evaluate_z = fXYZ_AnimationCurve.fZ.Evaluate (fCurrent_Time / fTotal_Time);

                    transform.localPosition = new Vector3 (fFrom.x + (fTo.x - fFrom.x) * evaluate_x, fFrom.y + (fTo.y - fFrom.y) * evaluate_y, fFrom.z + (fTo.z - fFrom.z) * evaluate_z);

                    yield return null;
                }

                if (fCurrent_Time >= fTotal_Time)
                {
                    fStatus = TweenStatus.Stop;
                }

                callback (fStatus);

                if (fLoop == false)
                {
                    break;
                }
                else
                {
                    fCurrent_Time = 0;
                }
            }
        }

        #region UI
        public void OnPointerDown (PointerEventData data)
        {
            if (transform.position.z < 50.0f && transform.position.z > 30.0f)
            {
                Destroy (gameObject);
            }
        }
        #endregion
    }
}