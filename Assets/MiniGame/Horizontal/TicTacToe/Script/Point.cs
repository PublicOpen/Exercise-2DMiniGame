﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.TicTacToe
{
    public class Point : MonoBehaviour
    {
        public Button fBtn_Point;
        private OXType fType;
        public OXType pType
        {
            get
            {
                return fType;
            }
            set
            {
                fType = value;
                GetComponent<Image> ().sprite = OXSystem.Instance.fSprite_OX[Convert.ToByte (fType)];

                if (fType == OXType.None)
                {
                    fBtn_Point.interactable = true;
                }
                else
                {
                    fBtn_Point.interactable = false;
                    OXSystem.Instance.fUnused_Points.Remove (this);
                    TicTacToe_GUI.Instance.pStatus = OXSystem.Instance.Judge (fType);
                }
            }
        }

        void Start ()
        {
            fBtn_Point.onClick.AddListener (OnClick_Point);
        }

        #region UI
        private void OnClick_Point ()
        {
            if (TicTacToe_GUI.Instance.pStatus == GameStatus.Player1)
            {
                pType = OXSystem.Instance.fPlayer1_Symbol;
            }
        }
        #endregion
    }
}