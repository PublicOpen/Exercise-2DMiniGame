﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapleStar.TicTacToe
{
    public class OXSystem : MonoBehaviour
    {
        public static OXSystem Instance;

        [Header ("Grid")]
        public Sprite[] fSprite_OX; // 0:None、1:O、2:X
        public Point[] fPoints; // 9宮格全按鈕，ReStart時，還原會用到
        public List<Point> fUnused_Points = new List<Point> (); // 計算用，每下一手-1

        [Header ("Player's Symbol")]
        public OXType fPlayer1_Symbol;
        public OXType fPlayer2_Symbol;

        void Awake ()
        {
            Instance = this;
        }

        public void Setup_GameStart ()
        {
            OXSystem.Instance.fUnused_Points.Clear ();

            foreach (Point point in fPoints)
            {
                point.pType = OXType.None;
                fUnused_Points.Add (point);
            }
        }

        public void Setup_PlayerSymbol (OXType player1_symbol, OXType player2_symbol)
        {
            fPlayer1_Symbol = player1_symbol;
            fPlayer2_Symbol = player2_symbol;
        }

        public GameStatus Judge (OXType symbol)
        {
            if (fPoints[0].pType == symbol && fPoints[1].pType == symbol && fPoints[2].pType == symbol ||
                fPoints[3].pType == symbol && fPoints[4].pType == symbol && fPoints[5].pType == symbol ||
                fPoints[6].pType == symbol && fPoints[7].pType == symbol && fPoints[8].pType == symbol ||
                fPoints[0].pType == symbol && fPoints[3].pType == symbol && fPoints[6].pType == symbol ||
                fPoints[1].pType == symbol && fPoints[4].pType == symbol && fPoints[7].pType == symbol ||
                fPoints[2].pType == symbol && fPoints[5].pType == symbol && fPoints[8].pType == symbol ||
                fPoints[0].pType == symbol && fPoints[4].pType == symbol && fPoints[8].pType == symbol ||
                fPoints[2].pType == symbol && fPoints[4].pType == symbol && fPoints[6].pType == symbol)
            {
                if (symbol == fPlayer1_Symbol)
                {
                    return GameStatus.Win;
                }
                else
                {
                    return GameStatus.Lose;
                }
            }
            else if (fUnused_Points.Count == 0) // 9格都下完
            {
                return GameStatus.Tie;
            }
            else if (symbol == fPlayer1_Symbol)
            {
                return GameStatus.Player2;
            }
            else //if (symbol == fPlayer2_Symbol)
            {
                return GameStatus.Player1;
            }
        }

        #region AI
        public void AI ()
        {
            StartCoroutine (_AI ());
        }

        private IEnumerator _AI ()
        {
            int i = UnityEngine.Random.Range (0, fUnused_Points.Count);

            yield return new WaitForSeconds (0.5f);

            fUnused_Points[i].pType = fPlayer2_Symbol;
        }
        #endregion
    }
}