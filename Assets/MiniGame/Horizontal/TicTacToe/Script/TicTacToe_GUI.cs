﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.TicTacToe
{
    public class TicTacToe_GUI : MonoBehaviour
    {
        public static TicTacToe_GUI Instance;
        private GameStatus fStatus;
        public GameStatus pStatus
        {
            get
            {
                return fStatus;
            }
            set
            {
                fStatus = value;

                switch (value)
                {
                    case GameStatus.Prepare:
                        fTxt_Message.text = "";
                        fBtn_PlayerFirst.interactable = true;
                        fBtn_ComputerFirst.interactable = true;
                        fBtn_Restart.interactable = false;

                        OXSystem.Instance.Setup_GameStart ();

                        break;
                    case GameStatus.Player1:
                        fTxt_Message.text = "輪到你";

                        break;
                    case GameStatus.Player2:
                        fTxt_Message.text = "輪到電腦";
                        OXSystem.Instance.AI ();

                        break;
                    case GameStatus.Win:
                        pWin++;
                        fTxt_Message.text = "<color=#ff0000ff>你贏了</color>";

                        break;
                    case GameStatus.Tie:
                        pTie++;
                        fTxt_Message.text = "<color=#008000ff>平手</color>";

                        break;
                    case GameStatus.Lose:
                        pLose++;
                        fTxt_Message.text = "<color=#000080ff>你GG了</color>";

                        break;
                }
            }
        }

        [Header ("View")]
        public Text fTxt_Message;

        [Header ("Scoreboard")]
        public Text fTxt_Win;
        private int pWin
        {
            get
            {
                return Convert.ToInt32 (fTxt_Win.text);
            }
            set
            {
                PlayerPrefs.SetInt ("Win", value);
                fTxt_Win.text = value.ToString ();
            }
        }
        public Text fTxt_Tie;
        private int pTie
        {
            get
            {
                return Convert.ToInt32 (fTxt_Tie.text);
            }
            set
            {
                PlayerPrefs.SetInt ("Tie", value);
                fTxt_Tie.text = value.ToString ();
            }
        }
        public Text fTxt_Lose;
        private int pLose
        {
            get
            {
                return Convert.ToInt32 (fTxt_Lose.text);
            }
            set
            {
                PlayerPrefs.SetInt ("Lose", value);
                fTxt_Lose.text = value.ToString ();
            }
        }

        [Header ("Right-Menu")]
        public Button fBtn_PlayerFirst;
        public Button fBtn_ComputerFirst;
        public Button fBtn_Restart;
        public Button fBtn_Afresh;

        [Space (10)]
        public Button fBtn_Exit;

        void Awake ()
        {
            Instance = this;
        }

        void Start ()
        {
            pWin = PlayerPrefs.GetInt ("Win");
            pTie = PlayerPrefs.GetInt ("Tie");
            pLose = PlayerPrefs.GetInt ("Lose");

            fBtn_PlayerFirst.onClick.AddListener (OnClick_PlayerFirst);
            fBtn_ComputerFirst.onClick.AddListener (OnClick_ComputerFirst);
            fBtn_Restart.onClick.AddListener (OnClick_Restart);
            fBtn_Afresh.onClick.AddListener (OnClick_Afresh);
            fBtn_Exit.onClick.AddListener (OnClick_Exit);

            pStatus = GameStatus.Prepare;
        }

        #region UI
        private void OnClick_PlayerFirst ()
        {
            fBtn_PlayerFirst.interactable = false;
            fBtn_ComputerFirst.interactable = false;
            fBtn_Restart.interactable = true;

            OXSystem.Instance.Setup_PlayerSymbol (OXType.O, OXType.X);

            pStatus = GameStatus.Player1;
        }

        private void OnClick_ComputerFirst ()
        {
            fBtn_PlayerFirst.interactable = false;
            fBtn_ComputerFirst.interactable = false;
            fBtn_Restart.interactable = true;

            OXSystem.Instance.Setup_PlayerSymbol (OXType.X, OXType.O);

            pStatus = GameStatus.Player2;
        }

        private void OnClick_Restart ()
        {
            pStatus = GameStatus.Prepare;
        }

        private void OnClick_Afresh ()
        {
            pWin = 0;
            pTie = 0;
            pLose = 0;
        }

        private void OnClick_Exit ()
        {
            Application.Quit ();
        }
        #endregion
    }
}