﻿namespace MapleStar.TicTacToe
{
    public enum GameStatus
    {
        Prepare,
        Player1,
        Player2,
        Win,
        Tie,
        Lose
    }

    public enum OXType : int
    {
        None = 0,
        O = 1,
        X = 2
    }
}