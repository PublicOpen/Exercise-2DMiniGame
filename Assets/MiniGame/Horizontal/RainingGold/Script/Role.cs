﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.RainingGold
{
    public class Role : MonoBehaviour
    {
        public static Role Instance;

        public Vector3 fStartingPoint;
        private const float fMoveSpeed = 0.7f;

        public Image fImg_Self;
        public Sprite[] fSprite_Exercise;
        public Rigidbody2D fRB_Self;

        private Coroutine fCoroutine_Animation_Exercise;

        void Awake ()
        {
            Instance = this;
        }

        void Update ()
        {
            if (RainingGold_GUI.Instance.pStatus == GameStatus.Playing)
            {
                #if UNITY_EDITOR
                if (Input.anyKey == false || Input.GetKey (KeyCode.RightArrow) && Input.GetKey (KeyCode.LeftArrow))
                {
                    fRB_Self.velocity = Vector2.zero;

                    return;
                }
                if (Input.GetKey (KeyCode.LeftArrow))
                {
                    transform.rotation = Quaternion.Euler (0, 0, 0);

                    fRB_Self.velocity = Vector2.left * fMoveSpeed;
                }
                if (Input.GetKey (KeyCode.RightArrow))
                {
                    transform.rotation = Quaternion.Euler (0, 180, 0);

                    fRB_Self.velocity = Vector2.right * fMoveSpeed;
                }
                #elif UNITY_ANDROID || UNITY_IOS
                if (Input.acceleration.x <= 0.05f|| Input.acceleration.x >= -0.05f)
                {
                fRB_Self.velocity = Vector2.zero;
                }
                if (Input.acceleration.x < -0.05f)
                {
                transform.rotation = Quaternion.Euler (0, 0, 0);

                fRB_Self.velocity = Vector2.left * fMoveSpeed;
                }
                if (Input.acceleration.x > 0.05f)
                {
                transform.rotation = Quaternion.Euler (0, 180, 0);

                fRB_Self.velocity = Vector2.right * fMoveSpeed;
                }
                #endif
            }
        }

        public void Ready ()
        {
            transform.localPosition = fStartingPoint;
            fCoroutine_Animation_Exercise = StartCoroutine (_Animation_Exercise ());
        }

        private IEnumerator _Animation_Exercise ()
        {
            while (true)
            {
                fImg_Self.sprite = fSprite_Exercise[0];
                yield return new WaitForSeconds (0.1f);

                fImg_Self.sprite = fSprite_Exercise[1];
                yield return new WaitForSeconds (0.1f);

                fImg_Self.sprite = fSprite_Exercise[2];
                yield return new WaitForSeconds (0.1f);

                fImg_Self.sprite = fSprite_Exercise[3];
                yield return new WaitForSeconds (0.1f);
            }
        }

        public void Stop_Animation_Exercise ()
        {
            StopCoroutine (fCoroutine_Animation_Exercise);
        }

        void OnTriggerEnter2D (Collider2D other)
        {
            if (other.GetComponent<Coin> () != null)
            {
                RainingGold_GUI.Instance.pScore += other.GetComponent<Coin> ().fCurrencyValue;
            }
        }
    }
}