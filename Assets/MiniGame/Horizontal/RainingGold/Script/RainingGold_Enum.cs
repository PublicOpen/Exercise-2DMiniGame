﻿namespace MapleStar.RainingGold
{
    public enum GameStatus
    {
        Description,
        CountDown,
        Playing,
        Pause,
        Result
    }

    public enum CoinType : int
    {
        Coin1 = 1,
        Coin5 = 5,
        Coin10 = 10
    }
}