﻿using UnityEngine;

namespace MapleStar.RainingGold
{
    public class Rain : MonoBehaviour
    {
        void Start () 
        {
            float range_x = (RainFallingSystem.Instance.fEnvironment.sizeDelta.x - GetComponent<RectTransform> ().sizeDelta.x) / 2;
            float y = (RainFallingSystem.Instance.fEnvironment.sizeDelta.y + GetComponent<RectTransform> ().sizeDelta.y) / 2;

            transform.localPosition = new Vector3 (UnityEngine.Random.Range (-range_x, range_x), y, 0);
        }

        void OnTriggerEnter2D (Collider2D other)
        {
            if (other.GetComponent<Rain> () == null)
            {
                Destroy (gameObject);
            }
        }
    }
}