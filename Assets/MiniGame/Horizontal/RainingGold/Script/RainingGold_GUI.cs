﻿using MapleStar.Timer;
using MapleStar.Tween;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.RainingGold
{
    public class RainingGold_GUI : MonoBehaviour
    {
        public static RainingGold_GUI Instance;
        private GameStatus fStatus;
        public GameStatus pStatus
        {
            get
            {
                return fStatus;
            }
            set
            {
                fStatus = value;

                switch (value)
                {
                    case GameStatus.Description:
                        fPanel_Description.gameObject.SetActive (true);

                        break;
                    case GameStatus.CountDown:
                        Time.timeScale = 1;

                        Reset_Game ();

                        fPanel_CountDown.gameObject.SetActive (true);
                        fPanel_CountDown.Play (0, (TweenStatus s) => {
                            RainFallingSystem.Instance.Begin_Rain ();
                            pStatus = GameStatus.Playing;
                        });

                        break;
                    case GameStatus.Playing:
                        Time.timeScale = 1;

                        fTime.Play ((TimerFinishStatus s) => {
                            if (s == TimerFinishStatus.Normal)
                            {
                                pStatus = GameStatus.Result;
                            }
                        });

                        break;
                    case GameStatus.Pause:
                        Time.timeScale = 0;

                        fTime.Pause ();
                        fPanel_Pause.gameObject.SetActive (true);

                        break;
                    case GameStatus.Result:
                        End_Game ();
                        fPanel_Result.gameObject.SetActive (true);

                        break;
                }
            }
        }

        [Header ("Header")]
        public Text fTxt_Time;
        public TimerCountDown fTime;
        private int pTime
        {
            get
            {
                return Convert.ToInt32 (fTxt_Time.text);
            }
            set
            {
                fTxt_Time.text = value.ToString ();
            }
        }
        public Text fTxt_Score;
        public int pScore
        {
            get
            {
                return Convert.ToInt32 (fTxt_Score.text);
            }
            set
            {
                fTxt_Score.text = value.ToString ();
            }
        }

        [Header ("Panel")]
        public DescriptionPanel fPanel_Description; // 遊戲說明畫面
        public CountDownPanel fPanel_CountDown; // 倒數畫面
        public GameObject fPanel_Pause;
        public ResultPanel fPanel_Result;

        void Awake ()
        {
            Instance = this;
        }

        void Start ()
        {
            fTime.GetTime ((double d) => {
                pTime = Convert.ToInt32 (d);
            });
            fTime.Setup (new TimeSpan (0, 0, 60));

            pStatus = GameStatus.Description;
        }

        void Update ()
        {
            /* 鍵盤事件 */
            if (Input.GetKeyDown (KeyCode.Escape))
            {
                if (pStatus == GameStatus.Playing)
                {
                    fPanel_Pause.SetActive (true);
                    pStatus = GameStatus.Pause;
                }
                else if (pStatus == GameStatus.Pause)
                {
                    fPanel_Pause.SetActive (false);
                    pStatus = GameStatus.Playing;
                }
            }
        }

        private void Reset_Game ()
        {
            pTime = Convert.ToInt32 (fTime.fTotal_Time);
            pScore = 0;

            Role.Instance.Ready ();
        }

        public void End_Game ()
        {
            fTime.Stop ();

            Role.Instance.Stop_Animation_Exercise ();

            RainFallingSystem.Instance.Stop_Rain ();
            RainFallingSystem.Instance.Clearance ();
        }
    }
}