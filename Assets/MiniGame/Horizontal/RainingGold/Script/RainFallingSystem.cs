﻿using System.Collections;
using UnityEngine;

namespace MapleStar.RainingGold
{
    public class RainFallingSystem : MonoBehaviour
    {
        public static RainFallingSystem Instance;

        public RectTransform fEnvironment; // 環境物件:雨要生成的圖層
        public GameObject[] fPrefab_RainObj;

        private float fFrequency = 1f; // 降雨的頻率
        private Coroutine fCoroutine_Raining;

        void Awake ()
        {
            Instance = this;
        }

        public void Begin_Rain ()
        {
            fCoroutine_Raining = StartCoroutine (_Raining ());
        }

        private IEnumerator _Raining ()
        {
            while (true)
            {
                Instantiate (fPrefab_RainObj[UnityEngine.Random.Range (0, fPrefab_RainObj.Length)], fEnvironment);

                yield return new WaitForSeconds (fFrequency);
            }
        }

        public void Stop_Rain ()
        {
            StopCoroutine (fCoroutine_Raining);
        }

        public void Clearance ()
        {
            Rain[] rains = fEnvironment.GetComponentsInChildren<Rain> ();

            foreach (Rain rain in rains)
            {
                Destroy (rain.gameObject);
            }
        }
    }
}