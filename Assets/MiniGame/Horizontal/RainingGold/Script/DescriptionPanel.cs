﻿using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.RainingGold
{
    public class DescriptionPanel : MonoBehaviour
    {
        public Button fBtn_Go;

        void Start () 
        {
            fBtn_Go.onClick.AddListener (OnClick_Go);
        }

        #region UI
        private void OnClick_Go ()
        {
            gameObject.SetActive (false);
            RainingGold_GUI.Instance.pStatus = GameStatus.CountDown;
        }
        #endregion
    }
}