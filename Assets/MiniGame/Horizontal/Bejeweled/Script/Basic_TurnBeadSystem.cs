﻿using MapleStar.Timer;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.Bejeweled
{
    public class Basic_TurnBeadSystem : MonoBehaviour
    {
        public static Basic_TurnBeadSystem Instance;
        public BejeweledSetting fSettingFile;
        private JewelStatus fStatus;
        public JewelStatus pStatus
        {
            get
            {
                return fStatus;
            }
            set
            {
                fStatus = value;

                switch (value)
                {
                    case JewelStatus.Generate:
                        Generate ();

                        break;
                    case JewelStatus.Fall:
                        Fall ();

                        break;
                    case JewelStatus.FallWaiting:
                        FallWaiting ();

                        break;
                    case JewelStatus.AutoEliminate:
                        AutoEliminateCheck ();

                        break;
                    case JewelStatus.CanExchange:


                        break;
                    case JewelStatus.Moving:
                        Swap (() => {
                            bool a = EliminateCheck (fSelected_Jewel);
                            bool b = EliminateCheck (fChanged_Jewel);

                            if (a == true || b == true)
                            {
                                fSelected_Jewel = null;
                                fChanged_Jewel = null;

                                pStatus = JewelStatus.Generate;
                            }
                            else
                            {
                                Swap (() => {
                                    fSelected_Jewel = null;
                                    fChanged_Jewel = null;

                                    pStatus = JewelStatus.CanExchange;
                                });
                            }
                        });

                        break;
                }
            }
        }
        public MultiObjectWaiting fMultiObjectWaiting;

        /// <summary>
        /// X
        /// </summary>
        private int fTotal_Column;
        /// <summary>
        /// Y
        /// </summary>
        private int fTotal_Row;

        [Header ("Jewel Matrix")]
        public RectTransform fJewel_Matrix;
        public RectTransform fSlot_Mask;

        [Header ("Game Pool Object")]
        public GameObject fPrefab_Slot;
        public GameObject fPrefab_Slot_Obstacle;
        public GameObject fPrefab_Jewel;

        [Header ("Record Use")]
        public Dictionary<string, Slot> fJewelSlots = new Dictionary<string, Slot> (); // 全Slot
        public ColumnSlots[] fOpen_ColumnSlots; // 各排有開放的Slot
        public List<Slot> fClose_Slots = new List<Slot> (); // 關閉的Slot

        [Space (10)]
        public Jewel fSelected_Jewel; // 第一個被點的寶石
        public Jewel fChanged_Jewel; // 第二個被點的寶石

        void Awake ()
        {
            Instance = this;
        }

        IEnumerator Start ()
        {
            fTotal_Column = Convert.ToInt32 (fSettingFile.fXY.x);
            fTotal_Row = Convert.ToInt32 (fSettingFile.fXY.y);
            fOpen_ColumnSlots = new ColumnSlots[fTotal_Column];

            for (int i = 0; i < fTotal_Column; i++)
            {
                ColumnSlots column_slots = new ColumnSlots ();
                column_slots.fC_Slot = new List<Slot> ();

                for (int j = 0; j < fTotal_Row; j++)
                {
                    GameObject obj = Instantiate (fPrefab_Slot, fJewel_Matrix);
                    Slot slot = obj.GetComponent<Slot> ();
                    slot.Setup (i, j, fSettingFile.fX_Column[i].fY_Row[j]);

                    fJewelSlots.Add (string.Format ("{0},{1}", i, j), slot);

                    if (slot.fIs_Open == true)
                    {
                        column_slots.fC_Slot.Add (slot);
                    }
                    else
                    {
                        fClose_Slots.Add (slot);
                    }
                    
                    #if UNITY_EDITOR
                    obj.name = string.Format ("{0},{1}", i, j);
                    #endif
                }

                fOpen_ColumnSlots[i] = column_slots;
            }

            yield return new WaitForEndOfFrame ();

            fJewel_Matrix.GetComponent<GridLayoutGroup> ().enabled = false;

            for (int i = 0; i < fOpen_ColumnSlots.Length; i++)
            {
                fOpen_ColumnSlots[i].fPosX = fOpen_ColumnSlots[i].fC_Slot[0].transform.localPosition.x;
            }
            foreach (Slot slot in fClose_Slots)
            {
                GameObject obstacle = Instantiate (fPrefab_Slot_Obstacle, fSlot_Mask);
                obstacle.transform.localPosition = slot.transform.localPosition;
            }

            pStatus = JewelStatus.Generate;
        }

        /// <summary>
        /// 生成珠寶
        /// </summary>
        public void Generate ()
        {
            StartCoroutine (_Generate ());
        }

        private IEnumerator _Generate ()
        {
            yield return new WaitForEndOfFrame ();

            for (int i = 0; i < fOpen_ColumnSlots.Length; i++)
            {
                fOpen_ColumnSlots[i].fTop_JewelList = new List<Jewel> ();
                fOpen_ColumnSlots[i].fBottom_JewelList = new List<Jewel> ();

                /* 判斷各排的Slot有沒有Jewel */
                for (int j = 0; j < fOpen_ColumnSlots[i].fC_Slot.Count; j++)
                {
                    /* Slot沒有Jewel : 上層的珠寶List存放新生成的Jewel */
                    if (fOpen_ColumnSlots[i].fC_Slot[j].transform.childCount == 0)
                    {
                        GameObject obj = Instantiate (fPrefab_Jewel, fJewel_Matrix);
                        Jewel jewel = obj.GetComponent<Jewel> ();
                        fOpen_ColumnSlots[i].fTop_JewelList.Add (jewel);
                        jewel.transform.localPosition = new Vector2 (fOpen_ColumnSlots[i].fPosX, (fJewel_Matrix.sizeDelta.y + fPrefab_Jewel.GetComponent<RectTransform> ().sizeDelta.y) / 2 + fPrefab_Jewel.GetComponent<RectTransform> ().sizeDelta.y * (fOpen_ColumnSlots[i].fTop_JewelList.IndexOf (jewel)));
                    }
                    /* Slot有Jewel : 下層的珠寶List存放該Jewel */
                    else
                    {
                        fOpen_ColumnSlots[i].fBottom_JewelList.Add (fOpen_ColumnSlots[i].fC_Slot[j].GetComponentInChildren<Jewel> ());
                    }
                }
            }

            pStatus = JewelStatus.Fall;
        }

        /// <summary>
        /// 珠寶落下
        /// </summary>
        public void Fall ()
        {
            foreach (ColumnSlots column_slots in fOpen_ColumnSlots)
            {
                column_slots.fBottom_JewelList.AddRange (column_slots.fTop_JewelList);

                /* fBottom_JewelList.Count 一定等於 fC_Slot.Count */
                for (int i = 0; i < column_slots.fC_Slot.Count; i++)
                {
                    column_slots.fBottom_JewelList[i].transform.SetParent (fJewel_Matrix);
                    column_slots.fBottom_JewelList[i].Fall (column_slots.fC_Slot[i]);
                }
            }

            pStatus = JewelStatus.FallWaiting;
        }

        public void FallWaiting ()
        {
            StartCoroutine (_FallWaiting ());
        }

        private IEnumerator _FallWaiting ()
        {
            foreach (string key in fJewelSlots.Keys)
            {
                while (fJewelSlots[key].fIs_Open == true && fJewelSlots[key].transform.childCount == 0) // 開放的Slot卻沒有Jewel子物件
                {
                    yield return null;
                }
            }

            pStatus = JewelStatus.AutoEliminate;
        }

        public void AutoEliminateCheck ()
        {
            bool check = false;

            foreach (string key in fJewelSlots.Keys)
            {
                Jewel jewel = fJewelSlots[key].GetComponentInChildren<Jewel> ();

                if (fJewelSlots[key].fIs_Open == true && jewel != null)
                {
                    List<Jewel> a = new List<Jewel> ();
                    List<Jewel> b = new List<Jewel> ();
                    List<Jewel> c = new List<Jewel> ();
                    List<Jewel> d = new List<Jewel> ();
                    bool e = false;

                    if (A (fJewelSlots[key], jewel, ref a) + C (fJewelSlots[key], jewel, ref c) >= 2)
                    {
                        Eliminate (a);
                        Eliminate (c);
                        e = true;
                    }
                    if (B (fJewelSlots[key], jewel, ref b) + D (fJewelSlots[key], jewel, ref d) >= 2)
                    {
                        Eliminate (b);
                        Eliminate (d);
                        e = true;
                    }

                    if (e == true)
                    {
                        check = true;
                        Destroy (jewel.gameObject);
                    }
                }
            }

            if (check == true)
            {
                pStatus = JewelStatus.Generate;
            }
            else
            {
                pStatus = JewelStatus.CanExchange;
            }
        }

        private void Swap (Action callback)
        {
            Transform temp_transform1 = fSelected_Jewel.transform.parent;
            Transform temp_transform2 = fChanged_Jewel.transform.parent;

            fSelected_Jewel.transform.SetParent (temp_transform2);
            fChanged_Jewel.transform.SetParent (temp_transform1);

            fMultiObjectWaiting.Wait (2, () => {
                callback ();
            });

            fSelected_Jewel.Swap (() => {
                fMultiObjectWaiting.pCount++;
            });
            fChanged_Jewel.Swap (() => {
                fMultiObjectWaiting.pCount++;
            });
        }

        public bool EliminateCheck (Jewel jewel)
        {
            List<Jewel> a = new List<Jewel> ();
            List<Jewel> b = new List<Jewel> ();
            List<Jewel> c = new List<Jewel> ();
            List<Jewel> d = new List<Jewel> ();
            bool e = false;

            if (A (jewel.GetComponentInParent<Slot> (), jewel, ref a) + C (jewel.GetComponentInParent<Slot> (), jewel, ref c) >= 2)
            {
                Eliminate (a);
                Eliminate (c);
                e = true;
            }
            if (B (jewel.GetComponentInParent<Slot> (), jewel, ref b) + D (jewel.GetComponentInParent<Slot> (), jewel, ref d) >= 2)
            {
                Eliminate (b);
                Eliminate (d);
                e = true;
            }

            if (e == true)
            {
                Destroy (jewel.gameObject);
            }
            return e;
        }

        public void Eliminate (List<Jewel> temp_listJewel)
        {
            foreach (Jewel jewel in temp_listJewel)
            {
                Destroy (jewel.gameObject);
            }
        }

        #region 4方位計算-連鎖寶石
        private int A (Slot slot, Jewel jewel, ref List<Jewel> temp_listJewel) // ↑
        {
            int sum = 0;

            for (int i = slot.fRow; i < fTotal_Row - 1; i++)
            {
                Jewel temp_jewel = fJewelSlots[string.Format ("{0},{1}", slot.fColumn, i + 1)].GetComponentInChildren<Jewel> ();

                if (temp_jewel != null)
                {
                    if (jewel.pType == temp_jewel.pType)
                    {
                        temp_listJewel.Add (temp_jewel);
                        sum++;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }

            return sum;
        }

        private int B (Slot slot, Jewel jewel, ref List<Jewel> temp_listJewel) // →
        {
            int sum = 0;

            for (int i = slot.fColumn; i < fTotal_Column - 1; i++)
            {
                Jewel temp_jewel = fJewelSlots[string.Format ("{0},{1}", i + 1, slot.fRow)].GetComponentInChildren<Jewel> ();

                if (temp_jewel != null)
                {
                    if (jewel.pType == temp_jewel.pType)
                    {
                        temp_listJewel.Add (temp_jewel);
                        sum++;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }

            return sum;
        }

        private int C (Slot slot, Jewel jewel, ref List<Jewel> temp_listJewel) // ↓
        {
            int sum = 0;

            for (int i = slot.fRow; i > 0; i--)
            {
                Jewel temp_jewel = fJewelSlots[string.Format ("{0},{1}", slot.fColumn, i - 1)].GetComponentInChildren<Jewel> ();

                if (temp_jewel != null)
                {
                    if (jewel.pType == temp_jewel.pType)
                    {
                        temp_listJewel.Add (temp_jewel);
                        sum++;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            
            return sum;
        }

        private int D (Slot slot, Jewel jewel, ref List<Jewel> temp_listJewel) // ←
        {
            int sum = 0;

            for (int i = slot.fColumn; i > 0; i--)
            {
                Jewel temp_jewel = fJewelSlots[string.Format ("{0},{1}", i - 1, slot.fRow)].GetComponentInChildren<Jewel> ();

                if (temp_jewel != null)
                {
                    if (jewel.pType == temp_jewel.pType)
                    {
                        temp_listJewel.Add (temp_jewel);
                        sum++;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }

            return sum;
        }
        #endregion
    }
}