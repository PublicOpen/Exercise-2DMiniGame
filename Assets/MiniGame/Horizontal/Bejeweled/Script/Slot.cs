﻿using UnityEngine;

namespace MapleStar.Bejeweled
{
    public class Slot : MonoBehaviour
    {
		public int fColumn;
        public int fRow;
		public bool fIs_Open;

        public void Setup (int column, int row, bool is_open)
        {
            fColumn = column;
            fRow = row;
            fIs_Open = is_open;
        }
    }
}