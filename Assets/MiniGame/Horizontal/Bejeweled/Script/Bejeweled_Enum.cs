﻿namespace MapleStar.Bejeweled
{
    public enum JewelStatus
    {
        Generate,
        Fall,
        FallWaiting,
        AutoEliminate,
        CanExchange,
        Moving,
    }

    public enum JewelType : int
    {
        Ruby = 1, // 紅寶石
        Emerald = 2, // 祖母綠寶石
        Sapphire = 3, // 藍寶石
        Citrine = 4, // 黃水晶 茶晶
        Amethyst = 5, // 紫水晶
        Ivory = 6, // 象牙
        Obsidian = 7, // 黑曜石
        Diamond = 8 // 鑽石
    }
}