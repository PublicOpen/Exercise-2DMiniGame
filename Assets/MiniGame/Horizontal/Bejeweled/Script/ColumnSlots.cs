﻿using System;
using System.Collections.Generic;

namespace MapleStar.Bejeweled
{
	[Serializable]
    public struct ColumnSlots
	{
		public float fPosX;
		public List<Slot> fC_Slot;
        public List<Jewel> fTop_JewelList; // 上層的珠寶List
        public List<Jewel> fBottom_JewelList; // 下層的珠寶List
	}
}