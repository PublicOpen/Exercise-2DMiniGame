﻿using MapleStar.Tween;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MapleStar.Bejeweled
{
    public class Jewel : TweenPosition, IPointerDownHandler, IPointerEnterHandler
    {
        public Vector3 fFrom;
        public Vector3 fTo;

        private JewelType fType;
        public JewelType pType
        {
            get
            {
                return fType;
            }
            set
            {
                fType = value;

                switch (fType)
                {
                    case JewelType.Ruby:
                        GetComponent<Image> ().color = Color.red;

                        break;
                    case JewelType.Emerald:
                        GetComponent<Image> ().color = Color.green;

                        break;
                    case JewelType.Sapphire:
                        GetComponent<Image> ().color = Color.blue;

                        break;
                    case JewelType.Citrine:
                        GetComponent<Image> ().color = Color.yellow;

                        break;
                    case JewelType.Amethyst:
                        GetComponent<Image> ().color = Color.magenta;

                        break;
                    case JewelType.Ivory:
                        GetComponent<Image> ().color = Color.gray;

                        break;
                    case JewelType.Obsidian:
                        GetComponent<Image> ().color = Color.black;

                        break;
                    case JewelType.Diamond:
                        GetComponent<Image> ().color = Color.white;

                        break;
                }
            }
        }

        void Start ()
        {
            pType = EnumEx.GetRandomEnum<JewelType> ();
        }

        public void Fall (Slot slot)
        {
            fFrom = transform.localPosition;
            fTo = slot.transform.localPosition;

            Play (0, (TweenStatus s) => {
                transform.SetParent (slot.transform);
            });
        }

        public void Swap (Action callback)
        {
            fFrom = transform.localPosition;
            fTo = Vector3.zero;

            Play (0, (TweenStatus s) => {
                callback ();
            });
        }

        protected override IEnumerator _Play (Action<TweenStatus> callback)
        {
            while (true)
            {
                fStatus = TweenStatus.Play;

                while (fCurrent_Time < fTotal_Time && fStatus == TweenStatus.Play)
                {
                    fCurrent_Time += Time.deltaTime * fSpeed;

                    float evaluate_x = fXYZ_AnimationCurve.fX.Evaluate (fCurrent_Time / fTotal_Time);
                    float evaluate_y = fXYZ_AnimationCurve.fY.Evaluate (fCurrent_Time / fTotal_Time);

                    transform.localPosition = new Vector3 (fFrom.x + (fTo.x - fFrom.x) * evaluate_x, fFrom.y + (fTo.y - fFrom.y) * evaluate_y, 0);

                    yield return null;
                }

                if (fCurrent_Time >= fTotal_Time)
                {
                    fStatus = TweenStatus.Stop;
                }

                callback (fStatus);

                if (fLoop == false)
                {
                    break;
                }
                else
                {
                    fCurrent_Time = 0;
                }
            }
        }

        #region UI
        public void OnPointerDown (PointerEventData data)
        {
            if (Basic_TurnBeadSystem.Instance.pStatus == JewelStatus.CanExchange)
            {
                Basic_TurnBeadSystem.Instance.fSelected_Jewel = this;
            }
        }

        public void OnPointerEnter (PointerEventData data)
        {
            if (Basic_TurnBeadSystem.Instance.pStatus == JewelStatus.CanExchange)
            {
                if (Basic_TurnBeadSystem.Instance.fSelected_Jewel != null && Basic_TurnBeadSystem.Instance.fSelected_Jewel != this)
                {
                    Basic_TurnBeadSystem.Instance.fChanged_Jewel = this;
                    Basic_TurnBeadSystem.Instance.pStatus = JewelStatus.Moving;
                }
            }
        }
        #endregion
    }
}