﻿using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.Bejeweled
{
    public class Bejeweled_GUI : MonoBehaviour
    {
        public static Bejeweled_GUI Instance;

        [Space (10)]
        public Button fBtn_Exit;

        void Awake ()
        {
            Instance = this;
        }

        void Start ()
        {
            fBtn_Exit.onClick.AddListener (OnClick_Exit);
        }

        #region UI
        private void OnClick_Exit ()
        {
            Application.Quit ();
        }
        #endregion
    }
}