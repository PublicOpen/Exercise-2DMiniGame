﻿using System;
using UnityEngine;

namespace MapleStar.Bejeweled
{
    [CreateAssetMenu (fileName = "Bejeweled Setting", menuName = "Custom ScriptableObject/Bejeweled/Bejeweled Setting")]
    public class BejeweledSetting : ScriptableObject
    {
        public Vector2 fXY = new Vector2 (10, 10);

        [Space (10)]
        public Matrix[] fX_Column;
    }

    [Serializable]
    public struct Matrix
    {
        public bool[] fY_Row;
    }
}