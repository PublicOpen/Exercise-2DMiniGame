﻿namespace MapleStar.PaperScissorsStone
{
    public enum GameResult
    {
        Win,
        Lose,
        Tie
    }

    public enum FistType : int
    {
        Paper = 0,
        Scissors = 1,
        Stone = 2
    }
}