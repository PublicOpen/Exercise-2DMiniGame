﻿using UnityEngine;

namespace MapleStar.PaperScissorsStone
{
    public class MoraSystem : MonoBehaviour
    {
        public static MoraSystem Instance;

        void Awake ()
        {
            Instance = this;
        }

        public GameResult Judge_2P (FistType player1_fist, FistType player2_fist)
        {
            if (player1_fist == FistType.Paper    && player2_fist == FistType.Scissors || 
                player1_fist == FistType.Scissors && player2_fist == FistType.Stone    || 
                player1_fist == FistType.Stone    && player2_fist == FistType.Paper)
            {
                return GameResult.Lose;
            }
            else if (player1_fist == FistType.Paper    && player2_fist == FistType.Stone || 
                     player1_fist == FistType.Scissors && player2_fist == FistType.Paper || 
                     player1_fist == FistType.Stone    && player2_fist == FistType.Scissors)
            {
                return GameResult.Win;
            }
            else
            {
                return GameResult.Tie;
            }
        }

        #region AI
        public FistType AI ()
        {
            return EnumEx.GetRandomEnum<FistType> ();
        }
        #endregion
    }
}