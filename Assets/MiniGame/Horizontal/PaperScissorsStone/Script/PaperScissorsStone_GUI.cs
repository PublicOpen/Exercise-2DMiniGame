﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace MapleStar.PaperScissorsStone
{
    public class PaperScissorsStone_GUI : MonoBehaviour
    {
        [Header ("View")]
        public Image fImg_Player1;
        public Image fImg_Player2;
        public Text fTxt_Result; // 猜拳的結果

        [Header ("Scoreboard")]
        public Text fTxt_Win;
        private int pWin
        {
            get
            {
                return Convert.ToInt32 (fTxt_Win.text);
            }
            set
            {
                PlayerPrefs.SetInt ("Win", value);
                fTxt_Win.text = value.ToString ();
            }
        }
        public Text fTxt_Lose;
        private int pLose
        {
            get
            {
                return Convert.ToInt32 (fTxt_Lose.text);
            }
            set
            {
                PlayerPrefs.SetInt ("Lose", value);
                fTxt_Lose.text = value.ToString ();
            }
        }

        [Header ("Fist")]
        public Button fBtn_Paper;
        public Button fBtn_Scissors;
        public Button fBtn_Stone;
        public Sprite[] fSprite_Fist; // 0:布, 1:剪刀, 2:石頭

        [Header ("Right-Menu")]
        public Button fBtn_Afresh;

        [Space (10)]
        public Button fBtn_Exit;

        void Start ()
        {
            pWin = PlayerPrefs.GetInt ("Win");
            pLose = PlayerPrefs.GetInt ("Lose");

            fBtn_Paper.onClick.AddListener (OnClick_Paper);
            fBtn_Scissors.onClick.AddListener (OnClick_Scissors);
            fBtn_Stone.onClick.AddListener (OnClick_Stone);
            fBtn_Afresh.onClick.AddListener (OnClick_Afresh);
            fBtn_Exit.onClick.AddListener (OnClick_Exit);
        }

        private void ResultShow (FistType player1_fist)
        {
            FistType player2_fist = MoraSystem.Instance.AI ();
            fImg_Player2.sprite = fSprite_Fist[Convert.ToByte (player2_fist)];

            switch (MoraSystem.Instance.Judge_2P (player1_fist, player2_fist))
            {
                case GameResult.Win:
                    pWin++;
                    fTxt_Result.text = "<color=#ff0000ff>Win</color>";

                    break;
                case GameResult.Tie:
                    fTxt_Result.text = "<color=#008000ff>Again</color>";

                    break;
                case GameResult.Lose:
                    pLose++;
                    fTxt_Result.text = "<color=#000080ff>Lose</color>";

                    break;
            }
        }

        #region UI
        private void OnClick_Paper ()
        {
            fImg_Player1.sprite = fSprite_Fist[Convert.ToByte (FistType.Paper)];
            ResultShow (FistType.Paper);
        }

        private void OnClick_Scissors ()
        {
            fImg_Player1.sprite = fSprite_Fist[Convert.ToByte (FistType.Scissors)];
            ResultShow (FistType.Scissors);
        }

        private void OnClick_Stone ()
        {
            fImg_Player1.sprite = fSprite_Fist[Convert.ToByte (FistType.Stone)];
            ResultShow (FistType.Stone);
        }

        private void OnClick_Afresh ()
        {
            pWin = 0;
            pLose = 0;
        }

        private void OnClick_Exit ()
        {
            Application.Quit ();
        }
        #endregion
    }
}